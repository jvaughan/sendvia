<?php

class Mhi_Sendvia_Helper_Debug extends Mage_Core_Helper_Abstract {

    /**
     * @return bool
     */
    public function isDebugEnabled()
    {
        return Mage::helper('mhi_sendvia/config')->getDebugEnabled();
    }

    /**
     * @param mixed $message
     */
    public function debug($message)
    {
        if (! $this->isDebugEnabled()) {
            return;
        }

        Mage::log($message, null, 'mhi_sendvia_debug.log', true);
    }
}