<?php

class Mhi_Sendvia_Helper_Quote extends Mage_Core_Helper_Abstract {

    const REGISTRY_KEY_QUOTE_ID = 'mhi_sendvia_last_quote_id';
    const REGISTRY_KEY_QUERY_ID = 'mhi_sendvia_last_query_id';

    /**
     * @param string $id
     */
    public function registerQuoteId($id)
    {
        Mage::unregister(self::REGISTRY_KEY_QUOTE_ID);
        Mage::register(self::REGISTRY_KEY_QUOTE_ID, $id);
    }

    /**
     * @return mixed
     */
    public function getLastQuoteId()
    {
        return Mage::registry(self::REGISTRY_KEY_QUOTE_ID);
    }

    /**
     * @param string $id
     */
    public function registerQueryId($id)
    {
        Mage::unregister(self::REGISTRY_KEY_QUERY_ID);
        Mage::register(self::REGISTRY_KEY_QUERY_ID, $id);
    }

    /**
     * @return mixed
     */
    public function getLastQueryId()
    {
        return Mage::registry(self::REGISTRY_KEY_QUERY_ID);
    }
}