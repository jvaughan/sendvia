<?php

class Mhi_Sendvia_Helper_Sendvia extends Mage_Core_Helper_Abstract {

    /**
     * @var Mhi_Sendvia_Model_Resource_Sendvia_Local_Carrier_Collection
     */
    protected $_carrierCollection;

    /**
     * @var Mhi_Sendvia_Model_Resource_Sendvia_Local_Carrier_Service_Collection
     */
    protected $_serviceCollection;

    /**
     * @return array
     */
    public function getAllCarrierIds() {
        return Mage::getModel('mhi_sendvia/sendvia_carrier')->getCollection()->getAllIds();
    }

    /**
     * @return array
     */
    public function getCarrierIdToNameMapping()
    {
        $mapping = array();
        foreach (Mage::getModel('mhi_sendvia/sendvia_local_carrier')->getCollection() as $carrier) {
            $mapping[ $carrier->getSendviaId() ] = $carrier->getName();
        }

        return $mapping;
    }

    public function updateLocalCarriersAndServices()
    {
        $this->_unsetCollections();

        foreach (Mage::getModel('mhi_sendvia/sendvia_carrier')->getCollection() as $remoteCarrier) {
            if ($localCarrier = $this->_getLocalCarrierBySvId($remoteCarrier->getId())) {
                if ($localCarrier->getName() != $remoteCarrier->getName()) {
                    $localCarrier->setName($remoteCarrier->getName())
                        ->save();
                }
            } else {
                $localCarrier = Mage::getModel('mhi_sendvia/sendvia_local_carrier')
                    ->setSendviaId($remoteCarrier->getId())
                    ->setName($remoteCarrier->getName())
                    ->save()
                ;
            }

            $serviceCollection = Mage::getModel('mhi_sendvia/sendvia_carrier_service')
                ->setParent('carrier', $remoteCarrier->getId())
                ->getCollection()
            ;

            foreach ($serviceCollection as $remoteService) {
                if ($localService = $this->_getLocalServiceBySvId($remoteService->getId())) {
                    if ($localService->getName() != $remoteService->getName()) {
                        $localService->setName( $remoteService->getName() )
                            ->save()
                        ;
                    }
                }
                else {
                    Mage::getModel('mhi_sendvia/sendvia_local_carrier_service')
                        ->setCarrierId($localCarrier->getId())
                        ->setSendviaId($remoteService->getId())
                        ->setName($remoteService->getName())
                        ->save()
                    ;
                }
            }
        }
    }

    /**
     * @param $sendviaId
     * @return null|Mhi_Sendvia_Model_Sendvia_Local_Carrier
     */
    protected function _getLocalCarrierBySvId($sendviaId)
    {
        $carrier = Mage::getModel('mhi_sendvia/sendvia_local_carrier')->load($sendviaId, 'sendvia_id');

        return $carrier->hasData() ? $carrier : null;
    }

    /**
     * @param string $sendviaId
     * @return null|Mhi_Sendvia_Model_Sendvia_Local_Carrier_Service
     */
    protected function _getLocalServiceBySvId($sendviaId)
    {
        $service = Mage::getModel('mhi_sendvia/sendvia_local_carrier_service')->load($sendviaId, 'sendvia_id');

        return $service->hasData() ? $service : null;
    }

    protected function _unsetCollections()
    {
        unset($this->_carrierCollection);
        unset($this->_serviceCollection);
        return $this;
    }
}