<?php

class Mhi_Sendvia_Helper_Configxml extends Mage_Core_Helper_Abstract {

    const CARRIER_GROUP_SORT_ORDER = 4;

    /**
     * @param Mage_Core_Model_Config $config
     */
    public function addCarrierGroups(Mage_Core_Model_Config $config)
    {
        foreach (Mage::helper('mhi_sendvia/sendvia')->getCarrierIdToNameMapping() as $carrierId => $carrierName) {
            $prefix = 'sections/carriers/groups/mhi_sendvia_' . $carrierId;

            $config->setNode($prefix, '');
            $config->getNode($prefix)->addAttribute('translate', 'label');

            $config->setNode("${prefix}/label",         'Sendvia - ' . $carrierName)
                ->setNode("${prefix}/frontend_type",    'text')
                ->setNode("${prefix}/sort_order",       self::CARRIER_GROUP_SORT_ORDER)
                ->setNode("${prefix}/show_in_default",  1)
                ->setNode("${prefix}/show_in_website",  1)
                ->setNode("${prefix}/show_in_store",    1)
            ;

            $this->_addField($config, $prefix, 'active', array(
                'label'             => 'Enabled',
                'frontend_type'     => 'Select',
                'source_model'      => 'adminhtml/system_config_source_yesno',
                'sort_order'        => 1,
            ));

            $this->_addField($config, $prefix, 'allowed_methods', array(
                'label'             => 'Allowed Methods',
                'frontend_type'     => 'Multiselect',
                'sort_order'        => 10,
                'source_model'      => 'mhi_sendvia/config_source_carrierservices',
                'can_be_empty'      => 1,
            ));

            $this->_addField($config, $prefix, 'sallowspecific', array(
                'label'             => 'Ship to specific countries',
                'frontend_type'     => 'Select',
                'sort_order'        => 20,
                'frontend_class'    => 'shipping-applicable-country',
                'source_model'      => 'adminhtml/system_config_source_shipping_allspecificcountries',
            ));

            $this->_addField($config, $prefix, 'specificcountry', array(
                'label'             => 'Ship to specific countries',
                'frontend_type'     => 'Multiselect',
                'sort_order'        => 30,
                'source_model'      => 'adminhtml/system_config_source_country',
                'can_be_empty'      => 1,
            ));
        }
    }

    /**
     * @param Mage_Core_Model_Config $config
     * @param string $prefix
     * @param string $name
     * @param array $properties
     * @return string
     */
    protected function _addField(Mage_Core_Model_Config $config, $prefix, $name, $properties)
    {
        $fieldNode = $prefix . '/' . 'fields' . '/' . $name;
        $config->setNode($fieldNode, '');
        $config->getNode($fieldNode)->addAttribute('translate', 'label');

        foreach ($properties as $property => $value) {
            $config->setNode("${fieldNode}/$property", $value);
        }

        foreach (array('show_in_default', 'show_in_website', 'show_in_store') as $showNode) {
            if (! (isset($properties[$showNode]))) {
                $config->setNode($fieldNode . '/' . $showNode, 1);
            }
        }

        return $fieldNode;
    }
}