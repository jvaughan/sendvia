<?php

class Mhi_Sendvia_Helper_Order extends Mage_Core_Helper_Abstract {

    const METHOD_PREFIX     = 'mhi_sendvia';
    const GUID_REGEX        = '[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}';

    /**
     * @param Mage_Sales_Model_Order $order
     * @return bool
     */
    public function isSendviaOrder(Mage_Sales_Model_Order $order)
    {
        return (bool) preg_match('/^' . self::METHOD_PREFIX . '/', $order->getShippingMethod());
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return string|null
     */
    public function getSendviaServiceId(Mage_Sales_Model_Order $order)
    {
        if (! $this->isSendviaOrder($order)) {
            return null;
        }
        $components = $this->_getMethodComponents($order);
        return $components['service_id'];
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return string|null
     */
    public function getSendviaQuoteId(Mage_Sales_Model_Order $order)
    {
        if (! $this->isSendviaOrder($order)) {
            return null;
        }
        return $order->getMhiSendviaQuoteId();
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    protected function _getMethodComponents(Mage_Sales_Model_Order $order)
    {
        $method = $order->getShippingMethod();
        $pattern = '/^(' . self::METHOD_PREFIX . ')_(' . self::GUID_REGEX . ')$/';

        preg_match($pattern, $method, $matches);

        return array(
            'prefix'        => $matches[1],
            'service_id'    => $matches[2],
        );
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return Mhi_Sendvia_Model_Sendvia_Quote
     */
    public function reQuote(Mage_Sales_Model_Order $order)
    {
        $svQuote = Mage::getModel('mhi_sendvia/sendvia_quote')
            ->setRequote(true)
            ->setId($order->getMhiSendviaQueryId())
            ->save()
        ;

        return $svQuote;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return bool
     */
    public function shouldAllowRequote(Mage_Sales_Model_Order $order)
    {
        $orderId = $order->getId();

        $quotes = Mage::getModel('mhi_sendvia/quote')->getCollection()
            ->addFieldToFilter('order_id', $orderId)
        ;

        if ($quotes->count() == 0) {
            return true;
        }

        $allQuotesInvalid = true;
        foreach ($quotes as $quote) {
            if (! $quote->isStatusInvalid()) {
                $allQuotesInvalid = false;
            }
        }
        if ($allQuotesInvalid) {
            return true;
        }

        return false;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return null|Mhi_Sendvia_Model_Quote
     */
    public function getBookedQuote(Mage_Sales_Model_Order $order)
    {
        $quotes = Mage::getModel('mhi_sendvia/quote')
            ->getCollection()
            ->addFieldToFilter('order_id', $order->getId())
            ->addFieldToFilter('status', array(
                array('eq'  => Mhi_Sendvia_Model_Quote::STATUS_BOOKED),
                array('eq'  => Mhi_Sendvia_Model_Quote::STATUS_PAID),
            ))
            ->load()
        ;
        if ($quotes->count() != 1) {
            return null;
        }
        else {
            return $quotes->getFirstItem();
        }
    }
}