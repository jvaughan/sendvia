<?php

class Mhi_Sendvia_Helper_Config extends Mage_Core_Helper_Abstract {

    const CONFIG_PATH_DEBUG_ENABLED             = 'carriers/mhi_sendvia/debug';
    const CONFIG_PATH_PAYMENT_METHOD            = 'carriers/mhi_sendvia/payment_method';
    const CONFIG_PATH_SANDBOX                   = 'carriers/mhi_sendvia/sandbox';
    const CONFIG_PATH_WEBSERVICE_BASE_URL       = 'mhi_sendvia/webservice/base_url';
    const CONFIG_PATH_WEBSERVICE_REST_PREFIX    = 'mhi_sendvia/webservice/rest_prefix';
    const CONFIG_PATH_WEBSERVICE_AUTH_PATH      = 'mhi_sendvia/webservice/auth_path';
    const CONFIG_PATH_WEBSERVICE_TIMEOUT        = 'mhi_sendvia/webservice/timeout';
    const CONFIG_PATH_WEBSERVICE_CLIENT_ID      = 'carriers/mhi_sendvia/webservice_client_id';
    const CONFIG_PATH_WEBSERVICE_CLIENT_SECRET  = 'carriers/mhi_sendvia/webservice_client_secret';

    const CONFIG_PATH_SENDVIA_CARRIER_PREFIX    = 'carriers/mhi_sendvia';

    /**
     * @param mixed $store
     * @return bool
     */
    public function getDebugEnabled($store = null) {
        return Mage::getStoreConfigFlag(self::CONFIG_PATH_DEBUG_ENABLED, $store);
    }

    /**
     * @param mixed $store
     * @return string
     */
    public function getWebServiceBaseUrl($store = null)
    {
        return Mage::getStoreConfig(self::CONFIG_PATH_WEBSERVICE_BASE_URL, $store);
    }

    /**
     * @param mixed $store
     * @return string
     */
    public function getWebserviceRestPrefix($store = null)
    {
        return Mage::getStoreConfig(self::CONFIG_PATH_WEBSERVICE_REST_PREFIX, $store);
    }

    public function getAuthPath($store = null) {
        return Mage::getStoreConfig(self::CONFIG_PATH_WEBSERVICE_AUTH_PATH, $store);
    }

    /**
     * @param mixed $store
     * @return string
     */
    public function getWebserviceUri($store = null)
    {
        return $this->getWebServiceBaseUrl($store) . $this->getWebserviceRestPrefix($store);
    }

    /**
     * @param mixed $store
     * @return string
     */
    public function getAuthUri($store = null) {
        return $this->getWebServiceBaseUrl($store) . $this->getAuthPath($store);
    }

    /**
     * @param mixed $store
     * @return mixed
     */
    public function getWebserviceTimeout($store = null)
    {
        return Mage::getStoreConfig(self::CONFIG_PATH_WEBSERVICE_TIMEOUT, $store);
    }

    /**
     * @param mixed $store
     * @return string
     */
    public function getClientId($store = null) {
        return Mage::getStoreConfig(self::CONFIG_PATH_WEBSERVICE_CLIENT_ID, $store);
    }

    /**
     * @param mixed $store
     * @return string
     */
    public function getClientSecret($store = null) {
        return Mage::getStoreConfig(self::CONFIG_PATH_WEBSERVICE_CLIENT_SECRET, $store);
    }

    /**
     * @param mixed $store
     * @return string
     */
    public function getPaymentMethod($store = null) {
        return Mage::getStoreConfig(self::CONFIG_PATH_PAYMENT_METHOD, $store);
    }

    /**
     * @param mixed $store
     * @return bool
     */
    public function isSandboxMode($store = null) {
        return Mage::getStoreConfigFlag(self::CONFIG_PATH_SANDBOX, $store);
    }

    /**
     * @param $carrierId
     * @param null $store
     * @return bool
     */
    public function isCarrierEnabled($carrierId, $store=null) {
        $node = $this->_getCarrierSpecificPath($carrierId, 'active');
        return Mage::getStoreConfigFlag($node, $store);
    }

    /**
     * @param $carrierId
     * @param null $store
     * @return bool
     */
    public function isServiceEnabled($carrierId, $serviceId, $store=null) {
        $node = $this->_getCarrierSpecificPath($carrierId, 'allowed_methods');
        $allowedMethods = explode(',', Mage::getStoreConfig($node, $store));

        return in_array($serviceId, $allowedMethods);
    }

    /**
     * @param string $carrierId
     * @param null|mixed $store
     * @return bool
     */
    public function isCarrierRestrictedToSpecificCountries($carrierId, $store = null) {
        $path = $this->_getCarrierSpecificPath($carrierId, 'sallowspecific');
        return Mage::getStoreConfigFlag($path, $store);
    }

    /**
     * @param string $carrierId
     * @param null|mixed $store
     * @return array
     */
    public function getCarrierAllowedCountries($carrierId, $store = null) {
        $path = $this->_getCarrierSpecificPath($carrierId, 'specificcountry');
        $value = Mage::getStoreConfig($path, $store);
        return $value ? explode(',', Mage::getStoreConfig($path, $store)) : array();
    }

    /**
     * @param string $field
     * @return string
     */
    protected function _getCarrierSpecificPath($carrierId, $field) {
        return self::CONFIG_PATH_SENDVIA_CARRIER_PREFIX . '_' . $carrierId . '/' . $field;
    }
}