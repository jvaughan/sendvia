<?php

class Mhi_Sendvia_Helper_Cache extends Mage_Core_Helper_Abstract {

    const CACHE_TAG = 'mhi_sendvia';

    /**
     * @param string $key
     * @param mixed $value
     * @param bool|integer $lifetime
     * @return bool
     */
    public function save($key, $value, $lifetime = false) {
        if ($this->isEnabled()) {
            Mage::app()->saveCache(
                serialize($value),
                $key,
                array(static::CACHE_TAG),
                $lifetime
            );
            return true;
        }

        return false;
    }

    /**
     * @param string $key
     * @return bool|mixed
     */
    public function load($key) {
        if ($this->isEnabled()) {
            $value = Mage::app()->loadCache($key);
            if ($value) {
                return unserialize($value);
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isEnabled() {
        return Mage::app()->useCache(static::CACHE_TAG);
    }
}