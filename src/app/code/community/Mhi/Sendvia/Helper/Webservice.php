<?php

class Mhi_Sendvia_Helper_Webservice extends Mage_Core_Helper_Abstract {

    const CACHE_KEY_AUTH_TOKEN = 'mhi_sendvia/webservice_auth_token';

    /**
     * @param bool $force
     * @return string
     */
    public function getToken($force = false)
    {
        $cacheHelper = Mage::helper('mhi_sendvia/cache');

        if (! $force) {
            $cachedValue = $cacheHelper->load(self::CACHE_KEY_AUTH_TOKEN);
            if ($cachedValue) {
                return $cachedValue;
            }
        }

        $response = Mage::getModel('mhi_sendvia/webservice_request_authentication')->requestAuthentication();
        if (! $response->isSuccess()) {
            Mage::throwException($response->getErrorMessage());
        }

        $token = $response->getItem()->getAccessToken();
        if (! $token) {
            Mage::throwException('No token returned by sendvia service');
        }

        $cacheHelper->save(self::CACHE_KEY_AUTH_TOKEN, $token, 3000);

        return $token;
    }

    /**
     * @return string
     */
    public function generateGuid() {
        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
        $charId = strtolower(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"

        $uuid =
             substr($charId, 0, 8).$hyphen
            .substr($charId, 8, 4).$hyphen
            .substr($charId,12, 4).$hyphen
            .substr($charId,16, 4).$hyphen
            .substr($charId,20,12)
        ;

        return $uuid;
    }

    /**
     * @param array|Varien_Object $data
     * @return array
     */
    public function prepareData($data)
    {
        if ($data instanceof Varien_Object) {
            $data = (array) $data->getData();
        }
        if (is_array($data)) {
            $new = array();
            foreach($data as $key => $val) {
                $new[ uc_words($key, '') ] = $this->prepareData($val);
            }
        }
        else {
            $new = $data;
        }

        return $new;
    }

    /**
     * @param array|Varien_Object $data
     * @return string
     */
    public function prepareDataAsJson($data) {
        return Zend_Json::encode(
            $this->prepareData($data)
        );
    }


    /**
     * @return Mhi_Sendvia_Model_Rest_Client
     */
    public function getRestClient()
    {
        $configHelper = Mage::helper('mhi_sendvia/config');

        $restClient = Mage::getModel('mhi_sendvia/rest_client')->setUri(
            $configHelper->getWebserviceUri()
        );

        $restClient->getHttpClient()->setConfig(array(
            'timeout'   => $configHelper->getWebserviceTimeout(),
        ));

        return $restClient;
    }
}