<?php

class Mhi_Sendvia_Helper_Data extends Mage_Core_Helper_Abstract {

    /**
     * @return string
     */
    public function getRedirectUrl()
    {
        return Mage::getModel('core/url')->getUrl('sendvia/paymentreturn');
    }

}