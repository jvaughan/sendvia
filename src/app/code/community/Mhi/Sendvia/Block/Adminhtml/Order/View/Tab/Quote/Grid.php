<?php

class Mhi_Sendvia_Block_Adminhtml_Order_View_Tab_Quote_Grid
    extends Mage_Adminhtml_Block_Widget_Grid
{

    protected $_rendererPrefix = 'mhi_sendvia/adminhtml_order_view_tab_quote_grid_column_renderer';

    public function __construct()
    {
        parent::__construct();

        $this->setId('sendvia_quote_grid');
        $this->setDefaultSort('carrier_id');
        $this->setUseAjax(true);
        $this->setFilterVisibility(false);
    }

    public function getTabClass()
    {
        return 'ajax';
    }

    public function getTabLabel() {
        return $this->__('Sendvia');
    }

    public function getTabTitle() {
        return $this->__('Sendvia');
    }

    public function canShowTab() {
        return Mage::helper('mhi_sendvia/order')->isSendviaOrder($this->getOrder());
    }

    public function isHidden() {
        return false;
    }

    /**
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        return Mage::registry('current_order');
    }

    /**
     * Prepare collection
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('mhi_sendvia/quote')
            ->getCollection()
            ->addFieldToFilter('order_id', $this->getOrder()->getId())
            ->setOrder('created_at')
        ;

        $collection->getSelect()->join(
            array('order_table'   => Mage::getSingleton('core/resource')->getTableName('sales/order')),
            'main_table.order_id = order_table.entity_id',
            array('order_table.shipping_method', 'order_table.shipping_description')
        );

        $collection->load();

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('quote_id', array(
            'header'    => $this->__('Quote Id'),
            'index'     => 'quote_id'
        ));


        $this->addColumn('created_at', array(
            'header'    => $this->__('Quoted On'),
            'index'     => 'created_at'
        ));

        $this->addColumn('service', array(
            'header'    => $this->__('Service'),
            'index'   => 'shipping_description'
        ));

        $this->addColumn('cost', array(
            'header'    => $this->__('Cost'),
            'index'     => 'cost',
            'renderer'  => $this->_rendererPrefix . '_cost',
        ));

        $this->addColumn('status', array(
            'header'    => $this->__('Status'),
            'index'     => 'status',
            'type'      => 'options',
            'options'   => Mage::getModel('mhi_sendvia/quote')->getAllStatuses(),
            'frame_callback' => array($this, 'decorateStatus')
        ));

        $this->addColumn('action', array(
            'header'    => $this->__('Action'),
            'index'     => 'status',
            'renderer'  => $this->_rendererPrefix . '_action',
        ));

        return parent::_prepareColumns();
    }

    public function decorateStatus($value, $row, $column, $isExport)
    {
        $status = $row->getStatus();

        switch($status) {
            case Mhi_Sendvia_Model_Quote::STATUS_BOOKED:
            case Mhi_Sendvia_Model_Quote::STATUS_PAID:
                $class = 'grid-severity-notice';
                break;

            case Mhi_Sendvia_Model_Quote::STATUS_UNBOOKED:
                $class = 'grid-severity-minor';
                break;

            case Mhi_Sendvia_Model_Quote::STATUS_INVALID:
                $class = 'grid-severity-critical';
                break;

            default:
                $class = null;
                break;
        }

        if ($class) {
            $cell = "<span class =\"$class\"><span>$value</span></span>";
        }
        else {
            $cell = $value;
        }

        return $cell;
    }
}