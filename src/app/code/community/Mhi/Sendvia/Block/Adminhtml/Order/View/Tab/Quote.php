<?php
class Mhi_Sendvia_Block_Adminhtml_Order_View_Tab_Quote
    extends Mage_Adminhtml_Block_Widget_Grid_Container
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    public function __construct()
    {
        $this->_controller  = 'adminhtml_order_view_tab_quote';
        $this->_blockGroup  = 'mhi_sendvia';

        $this->_headerText  = 'Sendvia';
        $this->_addButtonLabel = 'Request New Quote';

        parent::__construct();

        $this->_removeButton('add');

        if (Mage::helper('mhi_sendvia/order')->shouldAllowRequote($this->getOrder())) {
            $buttonConfMsg = $this->__('Are you sure you want to request another quote for this order?');
            $onclick = "if (confirm('${buttonConfMsg}')) {setLocation('" . $this->getCreateUrl() . "')}";

            $this->_addButton('add', array(
                'label'     => $this->_addButtonLabel,
                'onclick'   => $onclick,
                'class'     => 'add',
            ));
        }
    }

    public function getTabClass()
    {
        return 'ajax';
    }

    public function getTabLabel() {
        return $this->__('Sendvia');
    }

    public function getTabTitle() {
        return $this->__('Sendvia');
    }

    public function canShowTab() {
        return Mage::helper('mhi_sendvia/order')->isSendviaOrder($this->getOrder());
    }

    public function isHidden() {
        return false;
    }

    /**
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        return Mage::registry('current_order');
    }

    /**
     * @return string
     */
    public function getCreateUrl()
    {
        return Mage::getModel('adminhtml/url')->getUrl('adminhtml/sendvia/requote', array(
            'order_id' => $this->getOrder()->getId()
        ));
    }

}