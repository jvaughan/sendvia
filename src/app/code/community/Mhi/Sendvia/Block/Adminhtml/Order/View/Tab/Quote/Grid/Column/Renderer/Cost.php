<?php

class Mhi_Sendvia_Block_Adminhtml_Order_View_Tab_Quote_Grid_Column_Renderer_Cost
extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    /**
     * @param Varien_Object $row
     * @return string
     */
    public function render(Varien_Object $row)
    {
        $value = $this->_getValue($row);
        return Mage::helper('core')->currency($value, true, false);
    }
}