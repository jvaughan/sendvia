<?php

class Mhi_Sendvia_Block_Adminhtml_Order_View_Tab_Quote_Grid_Column_Renderer_Action
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    /**
     * @param Varien_Object $row
     * @return string
     */
    public function render(Varien_Object $row)
    {
        $status = $this->_getValue($row);

        switch($status) {
            case Mhi_Sendvia_Model_Quote::STATUS_UNBOOKED:
                $action  = 'book';
                $label   = 'Book';
                $confMsg = 'Are you sure you want to book this quote?';
                break;

            case Mhi_Sendvia_Model_Quote::STATUS_BOOKED:
                $action  = 'pay';
                $label   = 'Pay';
                $confMsg = 'Are you sure you want to pay for this quote?';
                break;

            case Mhi_Sendvia_Model_Quote::STATUS_PAID:
                $action = 'shippinglabel';
                $label  = 'View Shipping Label';
                break;

            default:
                $action = null;
                $label  = null;
        }

        if ($action && $label) {
            $url = Mage::getModel('adminhtml/url')->getUrl('adminhtml/sendvia/' . $action, array('id' => $row->getId()));
            if ($confMsg) {
                $confMsg = $this->__($confMsg);
                $onclick = "onclick=\"return confirm('${confMsg}')\"";
            }
            else {
                $onclick = '';
            }
            $value = "<a href=\"$url\" $onclick>" . $this->__($label) . "</a>";
        }
        else {
            $value = '';
        }

        return $value;
    }
}