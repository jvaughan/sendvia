<?php

class Mhi_Sendvia_Block_Adminhtml_System_Config_Form_Button_Getoptions extends Mage_Adminhtml_Block_System_Config_Form_Field {

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) {
        $this->setElement($element);

        $url = Mage::getModel('adminhtml/url')->getUrl('adminhtml/sendvia/getoptions');

        $html = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setType('button')
            ->setClass('scalable')
            ->setLabel('Get options')
            ->setOnClick("setLocation('$url')")
            ->toHtml();

        return $html;
    }
}