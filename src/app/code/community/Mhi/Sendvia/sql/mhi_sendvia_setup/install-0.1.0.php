<?php

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();

/**
 * Carrier table
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('mhi_sendvia/sendvia_local_carrier'))
    ->addColumn('carrier_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Id')

    ->addColumn('sendvia_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable'  => false,
    ), 'Sendvia Carrier Id')

    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable'  => false,
    ), 'Carrier name')
;
$installer->getConnection()->createTable($table);

/**
 * Carrier Service table
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('mhi_sendvia/sendvia_local_carrier_service'))
    ->addColumn('service_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Id')

    ->addColumn('carrier_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => 0,
    ), 'Parent Carrier ID')

    ->addColumn('sendvia_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable'  => false,
    ), 'Sendvia Carrier Id')

    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable'  => false,
    ), 'Service name')

    ->addIndex(
        $installer->getIdxName('mhi_sendvia/sendvia_local_carrier_service', array('carrier_id')),
        array('carrier_id')
    )

    ->addForeignKey(
        $installer->getFkName('mhi_sendvia/sendvia_local_carrier_service', 'carrier_id', 'mhi_sendvia/sendvia_local_carrier', 'carrier_id'),
        'carrier_id',
        $installer->getTable('mhi_sendvia/sendvia_local_carrier'),
        'carrier_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
;
$installer->getConnection()->createTable($table);


/**
 * Quotes table
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('mhi_sendvia/quote'))
    ->addColumn('quote_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Id')

    ->addColumn('order_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => 0,
    ), 'Parent Order ID')

    ->addColumn('sendvia_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable'  => false,
    ), 'Sendvia Carrier Id')

    ->addColumn('cost', Varien_Db_Ddl_Table::TYPE_FLOAT, null, array(
        'nullable'  => true,
    ), 'Cost of quote')

    ->addColumn('quote_data', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(
        'nullable'  => true,
    ), 'Serialized quote object')

    ->addColumn('receipt_id', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable'  => true,
    ), 'Serialized receipt object')

    ->addColumn('receipt_data', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(
        'nullable'  => true,
    ), 'Serialized receipt object')

    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_TINYINT, null, array(
        'nullable'  => false,
    ), 'Sendvia Carrier Id')

    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable'  => false,
    ), 'Created At')

    ->addIndex(
        $installer->getIdxName('mhi_sendvia/quote', array('order_id')),
        array('order_id')
    )

    ->addForeignKey(
        $installer->getFkName('mhi_sendvia/quote', 'carrier_id', 'sales/order', 'entity_id'),
        'order_id',
        $installer->getTable('sales/order'),
        'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE,
        Varien_Db_Ddl_Table::ACTION_CASCADE
    )
;
$installer->getConnection()->createTable($table);

/**
 * Add sendvia query ID field to sales/quote_address and sales/order tables
 */
$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address'), 'mhi_sendvia_query_id', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length'    => 64,
        'nullable'  => true,
        'comment'   => 'Sendvia query id'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'mhi_sendvia_query_id', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length'    => 64,
        'nullable'  => true,
        'comment'   => 'Sendvia query id'
    ));

/**
 * Add sendvia quote ID field to sales/quote_address and sales/order tables
 */
$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote_address'), 'mhi_sendvia_quote_id', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length'    => 64,
        'nullable'  => true,
        'comment'   => 'Sendvia quote id'
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'), 'mhi_sendvia_quote_id', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length'    => 64,
        'nullable'  => true,
        'comment'   => 'Sendvia quote id'
    ));

$installer->endSetup();