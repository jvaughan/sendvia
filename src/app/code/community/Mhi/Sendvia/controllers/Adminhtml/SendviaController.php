<?php

class Mhi_Sendvia_Adminhtml_SendviaController extends Mage_Adminhtml_Controller_Action {

    public function getoptionsAction()
    {
        try {
            Mage::helper('mhi_sendvia/sendvia')->updateLocalCarriersAndServices();

            $this->_getSession()->addSuccess(
                $this->__('Sendvia carriers and services have been updated')
            );
        }
        catch (Exception $e) {
            Mage::logException($e);

            $this->_getSession()->addError(
                $this->__('Sendvia carriers and services could not be updated')
            );
        }

        $this->_setRedirectToReferer();
    }

    public function bookAction()
    {
        $quoteId = $this->getRequest()->getParam('id');
        $quote = Mage::getModel('mhi_sendvia/quote')->load($quoteId);
        try {
            $quote->book();

            $this->_getSession()->addSuccess(
                $this->__('Sendvia quote has been booked')
            );
        }
        catch (Exception $e) {
            Mage::logException($e);

            $this->_getSession()->addError(
                $this->__('Could not make booking')
            );
        }

        $this->_setRedirectToSvTab($quote->getOrderId());
    }

    public function requoteAction()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        $order = Mage::getModel('sales/order')->load($orderId);
        $svQuote = Mage::helper('mhi_sendvia/order')->reQuote($order);

        $cost = $svQuote
            ->getShipmentByServiceId(Mage::helper('mhi_sendvia/order')->getSendviaServiceId($order))
            ->getCost()
        ;

        $quote = Mage::getModel('mhi_sendvia/quote')
            ->setData(array(
                'sendvia_id'    => $svQuote->getId(),
                'order_id'      => $orderId,
                'cost'          => $cost,
                'quote_data'    => serialize($svQuote),
                'status'        => Mhi_Sendvia_Model_Quote::STATUS_UNBOOKED,
            ))
            ->save()
        ;

        $this->_setRedirectToSvTab($orderId);
    }

    public function payAction() {
        $quoteId = $quoteId = $this->getRequest()->getParam('id');

        $paymentUrl = Mage::getModel('mhi_sendvia/quote')
            ->load($quoteId)
            ->getPaymentUrl();

        $this->getResponse()->setRedirect($paymentUrl);
    }

    public function shippinglabelAction()
    {
        $quoteId = $this->getRequest()->getParam('id');
        $quote = Mage::getModel('mhi_sendvia/quote')->load($quoteId);
        $pdf = $quote->getShippingLabelPdf();

        $this->getResponse()->setHeader(
            'Content-type', 'application/pdf'
        );

        $this->getResponse()->setBody($pdf);
    }

    protected function _setRedirectToSvTab($orderId = null) {
        if ($orderId) {
            $this->_redirect("adminhtml/sales_order/view", array(
                'order_id'      => $orderId,
                'active_tab'    => 'mhi_sendvia_order_view_tab_quote',
            ));
        }
        else {
            $this->_setRedirectToReferer();
        }
    }

    protected function _setRedirectToReferer() {
        $referer = Mage::helper('core/http')->getHttpReferer();

        $this->getResponse()->setRedirect(
            $referer ? $referer : Mage::getModel('adminhtml/url')->getUrl('adminhtml/dashboard')
        );
    }
}