<?php

class Mhi_Sendvia_PaymentController extends Mage_Core_Controller_Front_Action {

    const PAYMENT_SUCCESS_STRING    = 'success';

    public function returnAction()
    {
        $status    = $this->getRequest()->getParam('payment');
        $receiptId = $this->getRequest()->getParam('receipt');

        if ($status == self::PAYMENT_SUCCESS_STRING && $receiptId) {
            $quote = Mage::getModel('mhi_sendvia/quote')->load($receiptId, 'receipt_id');
            if (!$quote->hasData()) {
                $this->getResponse()->setHttpResponseCode(403);
            }

            $quote->setStatusToPaid();
            $quote->save();

            $this->_redirect("adminhtml/sales_order/view", array(
                'order_id'      => $quote->getOrderId(),
                'active_tab'    => 'mhi_sendvia_order_view_tab_quote',
            ));
        }
        else {
            $this->getResponse()->setHttpResponseCode(403);
        }
    }
}