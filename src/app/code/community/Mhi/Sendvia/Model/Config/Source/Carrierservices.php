<?php

class Mhi_Sendvia_Model_Config_Source_Carrierservices extends Varien_Object {

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = array();
        $desiredCarrierId = $this->_getCarrierIdFromConfigPath();

        $carriers = Mage::getModel('mhi_sendvia/sendvia_local_carrier')->getCollection();
        if ($desiredCarrierId !== null) {
            $carriers->addFieldToFilter('sendvia_id', $desiredCarrierId);
        }
        $carriers->load();

        foreach ($carriers as $carrier) {
            /** @var Mhi_Sendvia_Model_Sendvia_Local_Carrier $carrier */

            foreach ($carrier->getServices() as $service) {
                /** @var Mhi_Sendvia_Model_Sendvia_Local_Carrier_Service $service */
                $options[] = array(
                    'value'     => $service->getSendviaId(),
                    'label'     => $service->getName()
                );
            }
        }

        return $options;

    }

    /**
     * @return string|null
     */
    protected function _getCarrierIdFromConfigPath()
    {
        preg_match('/^carriers\/mhi_sendvia_([^\/]+)\/allowed_methods/', $this->getPath(), $matches);
        return (isset($matches[1]) && $matches[1]) ? $matches[1] : null;
    }
}