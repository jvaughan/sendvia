<?php

class Mhi_Sendvia_Model_Config_Source_Paymentmethods extends Varien_Object {

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value'   => 1,   'label'     => 'Paypal'),
            array('value'   => 2,   'label'     => 'Payment Order'),
            array('value'   => 3,   'label'     => 'Invoice'),
        );
    }
}