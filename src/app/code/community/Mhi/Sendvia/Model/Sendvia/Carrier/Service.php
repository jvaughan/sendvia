<?php

/**
 * Class Mhi_Sendvia_Model_Sendvia_Carrier_Service
 * @method string getName
 * @method Mhi_Sendvia_Model_Sendvia_Carrier_Service setName(string $name)
 */
class Mhi_Sendvia_Model_Sendvia_Carrier_Service extends Mhi_Sendvia_Model_Sendvia_Abstract {
    /**
     * @var string
     */
    protected $_name = 'service';

    /**
     * @var array
     */
    protected $_can = array(
        'read'
    );

    /**
     * @var array
     */
    protected $_requiredParents = array(
        'carrier'
    );
}