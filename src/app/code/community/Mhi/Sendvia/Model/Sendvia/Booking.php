<?php

/**
 * Class Mhi_Sendvia_Model_Sendvia_Booking
 * @method Mhi_Sendvia_Model_Sendvia_Booking setQuoteId(string $id)
 * @method string getQuoteId()
 *
 * @method Mhi_Sendvia_Model_Sendvia_Booking setPaymentMethod(integer $methodId)
 * @method integer getPaymentMethod()
 *
 * @method Mhi_Sendvia_Model_Sendvia_Booking setCurrency(integer $methodId)
 * @method integer getCurrency()
 */
class Mhi_Sendvia_Model_Sendvia_Booking extends Mhi_Sendvia_Model_Sendvia_Abstract {

    protected $_name = 'booking';

    /**
     * @var array
     */
    protected $_can = array(
        'read',
        'create',
    );

    /**
     * @var Mhi_Sendvia_Model_Sendvia_Booking_Receipt
     */
    protected $_receipt;

    public function getReceipt()
    {
        if (! isset($this->_receipt)) {
            $this->_receipt = Mage::getModel('mhi_sendvia/sendvia_booking_receipt');
        }

        return $this->_receipt;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return $this
     */
    public function buildFromOrder(Mage_Sales_Model_Order $order)
    {
        $this->setQuoteId(
                Mage::helper('mhi_sendvia/order')->getSendviaQuoteId($order)
            )->setPaymentMethod(
                Mage::helper('mhi_sendvia/config')->getPaymentMethod()
            )->setCurrency(
                Mage::helper('mhi_sendvia/locale')->getCurrencyCodeFrom3LetterAlpha($order->getGlobalCurrencyCode())
            );
        ;
    }

    /**
     * @param Mhi_Sendvia_Model_Quote $quote
     * @return $this
     */
    public function buildFromQuote(Mhi_Sendvia_Model_Quote $quote)
    {
        $order = $quote->getOrder();

        $serviceId = Mage::helper('mhi_sendvia/order')->getSendviaServiceId($order);
        $shipmentId = $quote->getSendviaQuote()
            ->getShipmentByServiceId($serviceId)
            ->getShipmentId()
        ;

        $this->setData(array(
            'quote_id'          => $quote->getSendviaId(),
            'payment_method'    => Mage::helper('mhi_sendvia/config')->getPaymentMethod(),
            'currency'          => Mage::helper('mhi_sendvia/locale')
                                    ->getCurrencyCodeFrom3LetterAlpha($order->getGlobalCurrencyCode()),

            'booking_shipments' => array(
                Mage::getModel('mhi_sendvia/sendvia_object')->setData(array(
                    'service_id'    => $serviceId,
                    'shipment'      => Mage::getModel('mhi_sendvia/sendvia_object')->setId($shipmentId)
                ))
            )
        ));

        return $this;
    }

    /**
     * @return Mhi_Sendvia_Model_Sendvia_Booking_Receipt
     */
    protected function _getModelToStoreSaveReturnDataIn() {
        return $this->getReceipt();
    }

    /**
     * @return Mhi_Sendvia_Model_Sendvia_Booking_Receipt
     */
    protected function _getModelToStoreLoadDataIn() {
        return $this->getReceipt();
    }
}