<?php

/**
 * Class Mhi_Sendvia_Model_Sendvia_Query
 *
 * @method Mhi_Sendvia_Model_Sendvia_Quote_Query setShipments(array $parcels)
 * @method array getShipments()
 *
 * @method Mhi_Sendvia_Model_Sendvia_Quote_Query setCurrency(integer $currency)
 * @method integer getCurrency()
 */
class Mhi_Sendvia_Model_Sendvia_Quote_Query extends Mhi_Sendvia_Model_Sendvia_Abstract {

    /**
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return $this
     */
    public function buildFromRateRequest(Mage_Shipping_Model_Rate_Request $request) {
        /**
         * @TODO set proper currency value
         */
        $this->setCurrency(826);

        $this->setShipments(array(
            Mage::getModel('mhi_sendvia/sendvia_shipment')
                ->giveNewId()
                ->addData(array(
                    /**
                     * @TODO use proper value for collection.
                     */
                    'collection'    => '2014-09-20',
                    'parcels'       => array(
                        Mage::getModel('mhi_sendvia/sendvia_shipment_parcel')
                            ->giveNewId()
                            ->setWeight(
                                (integer) Zend_Locale_Math::round($request->getPackageWeight())
                            )
                        ,
                    ),

                    'sender' => Mage::getModel('mhi_sendvia/sendvia_contact')
                        ->giveNewId()
                        ->setAddress(
                            Mage::getModel('mhi_sendvia/sendvia_contact_address')
                                ->giveNewId()
                                ->setCountryisoFrom2CharAlpha($request->getCountryId())
                        )
                    ,

                    'recipient' => Mage::getModel('mhi_sendvia/sendvia_contact')
                        ->giveNewId()
                        ->setAddress(
                            Mage::getModel('mhi_sendvia/sendvia_contact_address')
                                ->giveNewId()
                                ->setCity($request->getDestRegionCode())
                                ->setPostalArea($request->getDestPostcode())
                                ->setCountryisoFrom2CharAlpha($request->getDestCountryId())
                            )
                    ,
                ))
        ));

        return $this;
    }
}