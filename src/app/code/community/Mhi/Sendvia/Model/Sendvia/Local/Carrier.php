<?php

class Mhi_Sendvia_Model_Sendvia_Local_Carrier extends Mage_Core_Model_Abstract {

    protected function _construct()
    {
        $this->_init('mhi_sendvia/sendvia_local_carrier');
    }

    /**
     * @return Mhi_Sendvia_Model_Resource_Sendvia_Local_Carrier_Service_Collection
     */
    public function getServices()
    {
        $services = Mage::getModel('mhi_sendvia/sendvia_local_carrier_service')
            ->getCollection()
            ->addFieldToFilter('carrier_id', $this->getId())
            ->load()
        ;

        return $services;
    }
}