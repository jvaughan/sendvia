<?php

class Mhi_Sendvia_Model_Sendvia_Shipment_Label extends Mhi_Sendvia_Model_Sendvia_Abstract {

    /**
     * @var string
     */
    protected $_name = 'label';

    /**
     * @var array
     */
    protected $_can = array(
        'read'
    );

    /**
     * @var array
     */
    protected $_requiredParents = array(
        'shipment'
    );

    /**
     * @return string
     */
    protected function _getName()
    {
        return $this->_name;
    }
}