<?php

/**
 * Class Mhi_Sendvia_Model_Sendvia_Carrier
 * @method string getName
 * @method Mhi_Sendvia_Model_Sendvia_Carrier setName(string $name)
 */
class Mhi_Sendvia_Model_Sendvia_Carrier extends Mhi_Sendvia_Model_Sendvia_Abstract {

    protected $_name = 'carrier';

    /**
     * @var array
     */
    protected $_can = array(
        'read'
    );

    /**
     * @return Mhi_Sendvia_Model_Sendvia_Collection
     */
    public function getServices() {
        if (! $this->hasData('services')) {
            if (! $this->getId()) {
                return Mage::getModel('mhi_sendvia/sendvia_collection');
            }

            $services = Mage::getModel('mhi_sendvia/sendvia_carrier_service')
                ->setParent('carrier', $this->getId())
                ->getCollection()
            ;
            $this->setData('services', $services);
        }

        return $this->getData('services');
    }
}