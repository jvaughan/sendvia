<?php

abstract class Mhi_Sendvia_Model_Sendvia_Abstract extends Varien_Object {

    protected $_name = '';

    /**
     * Populate with 'create', 'read' 'update', 'delete',
     * depending on capabilities of Sendvia object and what we need to do with it.
     *
     * @var array
     */
    protected $_can = array();

    /**
     * @var array
     */
    protected $_requiredParents = array();

    /**
     * @var array
     */
    protected $_parents = array();

    /**
     * @var Mhi_Sendvia_Model_Webservice_Request
     */
    protected $_requestModel;

    /**
     * @return $this
     */
    public function giveNewId() {
        $this->setId(
            Mage::helper('mhi_sendvia/webservice')->generateGuid()
        );
        return $this;
    }

    /**
     * @param string $name
     * @param string $id
     * @return $this
     */
    public function setParent($name, $id)
    {
        $this->_parents[$name] = $id;
        return $this;
    }

    /**
     * @param array $parents
     * @return $this
     */
    public function setParents($parents = array())
    {
        foreach ($parents as $name => $id) {
            $this->setParent($name, $id);
        }

        return $this;
    }

    /**
     * @param null|string $id
     * @return $this
     */
    public function load($id = null)
    {
        if (! $this->canRead()) {
            return $this;
        }

        $path = $this->_getPath($id);
        $response = $this->getRequestModel()->read($path);

        $this->_getModelToStoreLoadDataIn()->setData(
            $response->getItem()->getData()
        );

        return $this;
    }

    /**
     * @return Mhi_Sendvia_Model_Sendvia_Collection
     */
    public function getCollection()
    {
        $collection = Mage::getModel('mhi_sendvia/sendvia_collection');

        if (! $this->canRead()) {
            return $collection;
        }

        $path = $this->_getPath(null);

        $response = $this->getRequestModel()->read($path);

        foreach ($response->getAllItems() as $item) {
        /** @var $item Mhi_Sendvia_Model_Sendvia_Object */
            $model = clone $this;
            $model->getModelToStoreLoadDataIn()->setData( $item->getData() );
            $collection->addItem($model);
        }

        return $collection;
    }

    /**
     * @return $this
     * @TODO handle updates as well as creates
     * @TODO handle exceptions
     */
    public function save() {
        if (! $this->canCreate()) {
            return $this;
        }

        $this->_beforeSave();

        $dataModel = $this->_getDataModelForCreate();

        if ($this->_useCreateMethod())  {
            if (! $dataModel->getId()) {
                $dataModel->giveNewId();
            }

            $response = $this->getRequestModel()->create(
                $this->_getPath(),
                $dataModel->getData()
            );

            $this->_getModelToStoreSaveReturnDataIn()->setData($response->getItem()->getData());
        }

        return $this;
    }

    /**
     * @return Mhi_Sendvia_Model_Webservice_Request
     */
    public function getRequestModel()
    {
        if (! isset($this->_requestModel)) {
            $this->_requestModel = Mage::getModel('mhi_sendvia/webservice_request');
        }

        return $this->_requestModel;
    }

    /**
     * @return bool
     */
    public function canRead()
    {
        return $this->_can('read');
    }

    /**
     * @return bool
     */
    public function canCreate()
    {
        return $this->_can('create');
    }

    /**
     * @param string $function
     * @return bool
     */
    protected function _can($function)
    {
        return in_array($function, $this->_can) && is_string($this->_name) && !empty($this->_name);
    }


    /**
     * @param null|string $id
     * @return string
     */
    protected function _getPath($id = null)
    {
        $path = array();
        $parents = $this->_getOrderedParents();

        foreach ($parents as $name => $parentId) {
            $path[] = $this->_pluralise($name);
            $path[] = $parentId;
        }

        $path[] = $this->_getName();
        if ($id !== null) {
            $path[] = $id;
        }

        return join('/', $path);
    }


    /**
     * @return array
     */
    protected function _getOrderedParents()
    {
        $orderedParents = array();
        foreach ($this->_requiredParents as $key) {
            $orderedParents[$key] = $this->_parents[$key];
        }

        return $orderedParents;
    }

    /**
     * @param string $parentName
     * @return null|string
     */
    public function getParentId($parentName)
    {
        return $this->_getParentId($parentName);
    }

    /**
     * @param string $parentName
     * @return string|null
     */
    protected function _getParentId($parentName) {
        if (isset($this->_parents[$parentName]) && $this->_parents[$parentName]) {
            return $this->_parents[$parentName];
        }
        else {
            return null;
        }
    }

    protected function _beforeSave()
    {

    }

    public function getModelToStoreLoadDataIn() {
        return $this->_getModelToStoreLoadDataIn();
    }

    /**
     * @return $this
     */
    protected function _getModelToStoreLoadDataIn() {
        return $this;
    }

    /**
     * @return $this
     */
    protected function _getModelToStoreSaveReturnDataIn() {
        return $this;
    }

    /**
     * @return Mhi_Sendvia_Model_Sendvia_Abstract
     */
    protected function _getDataModelForCreate() {
        return $this;
    }

    /**
     * @return bool
     */
    protected function _useCreateMethod()
    {
        if ($this->_getDataModelForCreate()->getId()) {
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * @param array $data
     * return array
     */
    protected function _processData($data) {
        $processed = array();
        foreach ($data as $k => $v) {
            $processed[ ucfirst($k) ] = $data[$k];
        }

        return $processed;
    }

    /**
     * @return string
     */
    protected function _getName() {
        return $this->_pluralise($this->_name);
    }

    /**
     * @param string $string
     * @return string
     */
    protected function _pluralise($string) {
        if (! preg_match('/s$/', $string)) {
            return $string . 's';
        }

        return $string;
    }
}