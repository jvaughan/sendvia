<?php

/**
 * Class Mhi_Sendvia_Model_Sendvia_Shipment
 * @method Mhi_Sendvia_Model_Sendvia_Shipment setParcels(array $parcels)
 * @method array getParcels()
 */
class Mhi_Sendvia_Model_Sendvia_Shipment extends Mhi_Sendvia_Model_Sendvia_Abstract {

    protected $_name = 'shipment';

    protected $_can = array(
        'read'
    );

    /**
     * @return array|mixed
     */
    public function getData($key = '', $index = null)
    {
        $this->setData('sandbox', Mage::helper('mhi_sendvia/config')->isSandboxMode());
        return parent::getData($key, $index);
    }

    /**
     * @return mixed
     */
    public function getShippingLabelPdf()
    {
        $label = Mage::getModel('mhi_sendvia/sendvia_shipment_label')
            ->setParent('shipment', $this->getId())
            ->getCollection()
            ->getFirstItem()
        ;

        return base64_decode($label->getData('data'));
    }
}