<?php

/**
 * Class Mhi_Sendvia_Model_Sendvia_Quote
 * @method array getQuoteShipments()
 */
class Mhi_Sendvia_Model_Sendvia_Quote extends Mhi_Sendvia_Model_Sendvia_Abstract {

    protected $_name = 'quote';

    /**
     * @var bool
     */
    protected $_isRequote = false;

    protected $_can = array(
        'create',
        'read',
    );

    /**
     * @var Mhi_Sendvia_Model_Sendvia_Quote_Query
     */
    protected $_query;

    /**
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return $this
     */
    public function makeRequest(Mage_Shipping_Model_Rate_Request $request)
    {
        $this->setQuery(
            Mage::getModel('mhi_sendvia/sendvia_quote_query')->buildFromRateRequest($request)
        );
        $this->save();

        if ($this->getId()) {
            Mage::helper('mhi_sendvia/quote')->registerQuoteId($this->getId());
            Mage::helper('mhi_sendvia/quote')->registerQueryId($this->getQuery()->getId());
        }

        return $this;
    }

    /**
     * @param string $serviceId
     * @return Mhi_Sendvia_Model_Sendvia_Abstract|null
     */
    public function getShipmentByServiceId($serviceId)
    {
        foreach ($this->getQuoteShipments() as $shipment) {
            if ($shipment->getService()->getId() == $serviceId) {
                return $shipment;
            }
        }

        return null;
    }

    /**
     * @param Mhi_Sendvia_Model_Sendvia_Quote_Query $query
     * @return $this
     */
    public function setQuery(Mhi_Sendvia_Model_Sendvia_Quote_Query $query)
    {
        $this->_query = $query;
        return $this;
    }

    /**
     * @return Mhi_Sendvia_Model_Sendvia_Quote_Query
     */
    public function getQuery()
    {
        if (!isset($this->_query)) {
            $this->_query = Mage::getModel('mhi_sendvia/sendvia_quote_query');
        }
        return $this->_query;
    }

    /**
     * @return bool
     */
    public function isRequote()
    {
        return $this->_isRequote;
    }

    /**
     * @param bool $val
     * @return $this
     */
    public function setRequote($val)
    {
        $this->_isRequote = $val;
        return $this;
    }

    /**
     * @return array
     */
    protected function _getDataModelForCreate() {
        return $this->isRequote() ? $this : $this->getQuery();
    }

    /**
     * @return bool
     */
    protected function _useCreateMethod() {
        return true;
    }
}