<?php

class Mhi_Sendvia_Model_Sendvia_Object extends Mhi_Sendvia_Model_Sendvia_Abstract {

    /**
     * @var string
     */
    protected $_name = 'object';

    /**
     * @var array
     */
    protected $_can = array();
}