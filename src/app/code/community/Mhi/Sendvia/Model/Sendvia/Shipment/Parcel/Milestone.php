<?php

class Mhi_Sendvia_Model_Sendvia_Shipment_Parcel_Milestone extends Mhi_Sendvia_Model_Sendvia_Abstract {

    protected $_name = 'milestone';

    protected $_can = array(
        'read'
    );

    protected $_requiredParents = array(
        'parcel'
    );
}