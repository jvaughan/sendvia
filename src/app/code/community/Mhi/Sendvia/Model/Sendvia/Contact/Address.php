<?php

class Mhi_Sendvia_Model_Sendvia_Contact_Address extends Mhi_Sendvia_Model_Sendvia_Abstract {

    /**
     * @param string $alpha
     * @return $this
     */
    public function setCountryisoFrom2CharAlpha($alpha) {
        $this->setCountryiso(
            Mage::helper('mhi_sendvia/locale')->getNumericCcFrom2LetterAlpha($alpha)
        );

        return $this;
    }
}