<?php

class Mhi_Sendvia_Model_Sendvia_Local_Carrier_Service extends Mage_Core_Model_Abstract {

    protected function _construct()
    {
        $this->_init('mhi_sendvia/sendvia_local_carrier_service');
    }
}