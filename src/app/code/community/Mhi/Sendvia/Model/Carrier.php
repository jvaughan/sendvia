<?php

class Mhi_Sendvia_Model_Carrier
    extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface

{
    protected $_code = 'mhi_sendvia';

    /**
     * @param  $request
     * @return Mage_Shipping_Model_Rate_Result
     */
    public function collectRates(
        Mage_Shipping_Model_Rate_Request $request
    )
    {
        if (! $this->isActive()) {
            return false;
        }

        $this->_debug('Package weight: ' . $request->getPackageWeight());

        $result = Mage::getModel('shipping/rate_result');
        if ($request->getFreeShipping()) {
            $result->append($this->_getFreeShippingRate());
        }

        try {
            $quote = Mage::getModel('mhi_sendvia/sendvia_quote')->makeRequest($request);
        }
        catch (Exception $e) {
            Mage::logException($e);
            return $result;
        }

        foreach ($quote->getQuoteShipments() as $quoteShipment) {
            $quoteCarrier = $quoteShipment->getCarrier();
            $quoteService = $quoteShipment->getService();

            if (! $this->_quoteMethodIsApplicable($quoteCarrier->getId(), $quoteService->getId(), $request)) {
                continue;
            }

            $carrierName = $quoteCarrier->getName();
            $serviceName = $quoteService->getName();

            $method = Mage::getModel('shipping/rate_result_method');
            $method->setData(array(
                'carrier'       => $this->_code,
                'carrier_title' => $carrierName,
                'method'        => $quoteShipment->getService()->getId(),
                'method_title'  => $carrierName . ': ' . $serviceName,
                'price'         => $quoteShipment->getCost(),
                'cost'          => 0,
            ));

            $result->append($method);
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getAllowedMethods()
    {
        $methods = array();

        foreach (Mage::getModel('mhi_sendvia/sendvia_local_carrier_service')->getCollection() as $service) {
            $carrierName = Mage::getModel('mhi_sendvia/sendvia_local_carrier')
                ->load($service->getCarrierId())
                ->getName()
            ;

            $methods['mhi_sendvia_' . $service->getSendviaId()] = $carrierName . ': ' . $service->getName();
        }

        return $methods;
    }

    /**
     * @return bool
     */
    public function isTrackingAvailable()
    {
        return true;
    }

    /**
     * @param string $tracking
     * @return Mhi_Sendvia_Model_Tracking_Result
     */
    public function getTrackingInfo($tracking)
    {
        $result = Mage::getModel('mhi_sendvia/tracking_result');
        $result->setTracking($tracking);

        $status = 'Tracking information not available';

        try {
            $shipment = Mage::getModel('mhi_sendvia/sendvia_shipment')->load($tracking);
            $parcels = $shipment->getParcels();

            if (count($parcels) == 1) {
                $parcel = $parcels[0];
                $milestones = Mage::getModel('mhi_sendvia/sendvia_shipment_parcel_milestone')
                    ->setParent('parcel', $parcel->getId())
                    ->getCollection()
                ;

                $status = "<ul>";
                foreach ($milestones as $milestone) {
                    $date = Mage::app()->getLocale()->storeDate(
                        null, $milestone->getDate(), true
                    );
                    $status .= '<li><strong>' . $date->toString(Zend_Date::DATETIME_SHORT) . ':</strong> ' . $milestone->getMessage() . '</li>';
                }
                $status .= '</ul>';
            }
        }
        catch (Exception $e) {
            Mage::logException($e);
        }

        $result->setStatus($status);


        return $result;
    }

    /**
     * @param string $carrierId
     * @param string $serviceId
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return bool
     */
    protected function _quoteMethodIsApplicable($carrierId, $serviceId, Mage_Shipping_Model_Rate_Request $request)
    {
        $configHelper = Mage::helper('mhi_sendvia/config');

        if (! $configHelper->isCarrierEnabled($carrierId)) {
            return false;
        }

        if (! $configHelper->isServiceEnabled($carrierId, $serviceId)) {
            return false;
        }

        if ($configHelper->isCarrierRestrictedToSpecificCountries($carrierId)) {
            $allowedCountries = $configHelper->getCarrierAllowedCountries($carrierId);
            if (!in_array($request->getDestCountryId(), $allowedCountries)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return Mage_Shipping_Model_Rate_Result_Method
     */
    protected function _getFreeShippingRate()
    {
        $rate = Mage::getModel('shipping/rate_result_method');
        /* @var $rate Mage_Shipping_Model_Rate_Result_Method */
        $rate->setCarrier($this->_code);
        $rate->setCarrierTitle($this->getConfigData('title'));
        $rate->setMethod('free_shipping');
        $rate->setMethodTitle('Free Shipping');
        $rate->setPrice(0);
        $rate->setCost(0);
        return $rate;
    }

    protected function _debug($msg)
    {
        Mage::helper('mhi_sendvia/debug')->debug($msg);
    }
}