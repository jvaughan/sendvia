<?php

class Mhi_Sendvia_Model_Webservice_Request extends Varien_Object {

    /**
     * @var Zend_Rest_Client
     */
    protected $_restClient;

    /**
     * @param string $path
     * @param array $data
     * @return Mhi_Sendvia_Model_Webservice_Response
     * @TODO
     */
    public function create($path, $data = array())
    {
        return $this->_makeRequest(
            'restPost',
            $path,
            Mage::helper('mhi_sendvia/webservice')->prepareDataAsJson($data)
        );
    }

    /**
     * @param string $path
     * @param array $data
     * @return Mhi_Sendvia_Model_Webservice_Response
     */
    public function read($path, $data = array())
    {
        return $this->_makeRequest('restGet', $path, $data);
    }

    /**
     * @param null $type
     * @param null $id
     * @param null $data
     * @TODO
     */
    public function update($type = null, $id = null, $data = null)
    {

    }

    /**
     * @param null $type
     * @param null $id
     * @TODO
     */
    public function delete($type = null, $id = null)
    {

    }

    /**
     * @param stdClass $restClient
     */
    public function setRestClient(stdClass $restClient)
    {

    }

    /**
     * @param string $method
     * @param string $path
     * @param array|string $data
     * @return Mhi_Sendvia_Model_Webservice_Response
     */
    protected function _makeRequest($method, $path, $data)
    {
        $restClient = $this->getRestClient();
        $this->_setToken();

        for ($i = 0; $i <= 1; $i++) {
            $httpResponse = $restClient->$method(
                $this->_getPrefixedPath($path),
                $data
            );

            $this->_debug($restClient->getHttpClient()->getLastRequest());
            $this->_debug($httpResponse);

            $response = Mage::getModel('mhi_sendvia/webservice_response')
                ->setHttpResponse($httpResponse);

            if ($response->isSuccess()) {
                return $response;
            }

            if ($response->isAuthenticationFailure()) {
                if ($i == 0) {
                    $this->_setToken(true);
                }
                else {
                    Mage::throwException('Sendvia authentication failed: ' . $response->getErrorMessage());
                }
            }
            else {
                Mage::throwException('Sendvia request failed: ' . $response->getErrorMessage());
            }
        }
    }

    /**
     * @param bool $force
     */
    protected function _setToken($force = false) {
        $token = Mage::helper('mhi_sendvia/webservice')->getToken($force);

        $this->getRestClient()->getHttpClient()->setHeaders(
            'Authorization',
            'Bearer' . ' ' . $token
        );
        return $this;
    }

    protected function _getPrefixedPath($path)
    {
        return Mage::helper('mhi_sendvia/config')->getWebserviceRestPrefix() . '/' . $path;
    }

    /**
     * @return Zend_Rest_Client
     */
    public function getRestClient()
    {
        if (! isset($this->_restClient)) {
            $this->_restClient = Mage::helper('mhi_sendvia/webservice')->getRestClient();
        }

        return $this->_restClient;
    }

    protected function _debug($message)
    {
        Mage::helper('mhi_sendvia/debug')->debug($message);
    }
}