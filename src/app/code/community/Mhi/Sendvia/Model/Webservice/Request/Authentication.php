<?php

class Mhi_Sendvia_Model_Webservice_Request_Authentication extends Mhi_Sendvia_Model_Webservice_Request {

    /**
     *
     */
    public function requestAuthentication() {
        $configHelper = Mage::helper('mhi_sendvia/config');
        $restClient = $this->getRestClient();

        $httpResponse = $restClient->restPost(
            $configHelper->getAuthPath(),
            array(
                'grant_type'    => 'client_credentials',
                'client_id'     => $configHelper->getClientId(),
                'client_secret' => $configHelper->getClientSecret(),
            )
        );

        $this->_debug($restClient->getHttpClient()->getLastRequest());
        $this->_debug($httpResponse);

        $response = Mage::getModel('mhi_sendvia/webservice_response')->setHttpResponse($httpResponse);
        return $response;
    }

    protected function _debug($msg)
    {
        Mage::helper('mhi_sendvia/debug')->debug($msg);
    }
}