<?php

/**
 * Class Mhi_Sendvia_Model_Webservice_Response
 * @method Zend_Http_Response getHttpResponse()
 * @method Mhi_Sendvia_Model_Webservice_Response setHttpResponse()
 */
class Mhi_Sendvia_Model_Webservice_Response extends Varien_Object {

    /**
     * @return bool
     */
    public function isSuccess()
    {
        $success = false;

        $response = $this->getHttpResponse();
        if (in_array($response->getStatus(), $this->_getSuccessfulHttpStatuses())) {
            $success = true;
        }

        return $success;
    }

    /**
     * @return bool
     */
    public function isAuthenticationFailure()
    {
        if (in_array($this->getHttpResponse()->getStatus(), $this->_getAuthFailureHttpStatuses())) {
            return true;
        }
        else {
            return false;
        }
    }

    public function getErrorMessage()
    {
        $message = '';
        if (! $this->isSuccess()) {
            $message = 'Unknown Error';

            $body = (array)$this->_getDecodedBody();
            if (isset($body['Message']) && $body['Message']) {
                $message = $body['Message'];
            }
            if (isset($body['ExceptionMessage']) && $body['ExceptionMessage']) {
                $message .= ' '. $body['ExceptionMessage'];
            }
        }

        return $message;
    }

    /**
     * Return Varien_Object representation of webservice response data.
     * @return Mhi_Sendvia_Model_Sendvia_Object
     */
    public function getItem()
    {
        $items = $this->getAllItems();
        return $items[0];
    }

    /**
     * @return array of Mhi_Sendvia_Model_Sendvia_Object
     */
    public function getAllItems()
    {
        $responseBody = $this->_getDecodedBody();

        if (is_array($responseBody)) {
            $responseItems = $responseBody;
        }
        else {
            $responseItems = array($responseBody);
        }

        return $this->_convertToSendviaObjects($responseItems);
    }

    /**
     * @param array $data
     * @return array|Varien_Object
     */
    protected function _convertToSendviaObjects($data)
    {
        if ($data instanceof stdClass) {
            $underscoreSeparated = array();
            foreach ((array) $data as $k => $v) {
                $underscoreSeparated [$this->_convertKeyToUnderscoreSeparated($k)] = $v;
            }

            $data = Mage::getModel('mhi_sendvia/sendvia_object')->setData($underscoreSeparated);
        }

        if ($data instanceof Mhi_Sendvia_Model_Sendvia_Object) {
            foreach ($data->getData() as $k => $v) {
                $data->setData($k, $this->_convertToSendviaObjects($v));
            }
        }

        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $data[$k] = $this->_convertToSendviaObjects($v);
            }
        }

        return $data;
    }

    /**
     * @param string $key
     * @return string
     */
    protected function _convertKeyToUnderscoreSeparated($key)
    {
        return strtolower(preg_replace('/\B([A-Z]+)/', "_$1", $key));
    }


    /**
     * @return mixed
     */
    protected function _getDecodedBody()
    {
        return json_decode($this->getHttpResponse()->getBody());
    }

    protected function _getAuthFailureHttpStatuses() {
        return array(401);
    }

    /**
     * @return array
     */
    protected function _getSuccessfulHttpStatuses()
    {
        return array(200);
    }
}