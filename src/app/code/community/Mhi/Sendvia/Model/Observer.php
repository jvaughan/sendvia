<?php

class Mhi_Sendvia_Model_Observer {

    public function adminhtmlInitSystemConfig(Varien_Event_Observer $observer)
    {
        Mage::helper('mhi_sendvia/debug')->debug('observing in ' . __METHOD__);

        /**
         * @var Mage_Core_Model_Config $config
         */
        $config = $observer->getEvent()->getConfig();
        Mage::helper('mhi_sendvia/configxml')->addCarrierGroups($config);
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function salesOrderPlaceAfter(Varien_Event_Observer $observer)
    {
        /**
         * @var Mage_Sales_Model_Order $order
         */
        $order = $observer->getEvent()->getOrder();

        $orderHelper = Mage::helper('mhi_sendvia/order');
        if (! $orderHelper->isSendviaOrder($order)) {
            return;
        }

        $this->_debug('observing in ' . __METHOD__);

        $svQuoteId = Mage::helper('mhi_sendvia/order')->getSendviaQuoteId($order);

        $quote = Mage::getModel('mhi_sendvia/quote')
            ->setOrderId($order->getId())
            ->setSendviaId($svQuoteId)
            ->setCost($order->getShippingAmount())
            ->setQuoteData(
                serialize(Mage::getModel('mhi_sendvia/sendvia_quote')->load($svQuoteId))
            )
            ->setStatus(Mhi_Sendvia_Model_Quote::STATUS_UNBOOKED)
        ;

        $quote->save();
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function salesQuoteAddressSaveBefore(Varien_Event_Observer $observer)
    {
        $this->_debug('observing in ' . __METHOD__);

        /**
         * @var Mage_Sales_Model_Quote_Address $address
         */
        $address = $observer->getEvent()->getQuoteAddress();

        $lastQuoteId = Mage::helper('mhi_sendvia/quote')->getLastQuoteId();
        if ($lastQuoteId) {
            $address->setData('mhi_sendvia_quote_id', $lastQuoteId);
        }

        $lastQueryId = Mage::helper('mhi_sendvia/quote')->getLastQueryId();
        if ($lastQueryId) {
            $address->setData('mhi_sendvia_query_id', $lastQueryId);
        }
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function salesOrderShipmentSaveAfter(Varien_Event_Observer $observer)
    {
        /**
         * @var Mage_Sales_Model_Order_Shipment $shipment
         */
        $shipment = $observer->getEvent()->getShipment();

        $carrierCode = Mage::getModel('mhi_sendvia/carrier')->getCarrierCode();
        if (! Mage::helper('mhi_sendvia/order')->isSendviaOrder($shipment->getOrder())) {
            return;
        }

        $this->_debug('observing in ' . __METHOD__);

        $svQuote = Mage::helper('mhi_sendvia/order')->getBookedQuote($shipment->getOrder());
        if (! $svQuote) {
            return;
        }

        $currentTracks = Mage::getModel('sales/order_shipment_track')
            ->getCollection()
            ->addFieldToFilter('parent_id', $shipment->getId())
            ->load()
        ;
        if ($currentTracks->count() > 0) {
            return;
        }

        Mage::getModel('sales/order_shipment_track')
            ->setParentId($shipment->getId())
            ->setCarrierCode($carrierCode)
            ->setOrderId($shipment->getOrderId())
            ->setTrackNumber($svQuote->getShipmentId())
            ->save()
        ;
    }

    protected function _debug($msg)
    {
        Mage::helper('mhi_sendvia/debug')->debug($msg);
    }
}