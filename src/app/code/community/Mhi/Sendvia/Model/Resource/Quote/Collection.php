<?php
/**
 * Created by PhpStorm.
 * User: jvaughan
 * Date: 25/09/2014
 * Time: 17:19
 */ 
class Mhi_Sendvia_Model_Resource_Quote_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    protected function _construct()
    {
        $this->_init('mhi_sendvia/quote');
    }

}