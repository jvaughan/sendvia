<?php

class Mhi_Sendvia_Model_Resource_Sendvia_Local_Carrier extends Mage_Core_Model_Resource_Db_Abstract {

    protected function _construct() {
        $this->_init('mhi_sendvia/sendvia_local_carrier', 'carrier_id');
    }
}