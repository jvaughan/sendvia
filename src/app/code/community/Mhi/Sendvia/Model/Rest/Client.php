<?php

class Mhi_Sendvia_Model_Rest_Client extends Zend_Rest_Client {

    const CONTENT_TYPE_APPLICATION_JSON = 'application/json';

    /**
     * @param array $args
     */
    public function __construct($args) {
        parent::__construct();
    }

    /**
     * Perform a POST or PUT
     *
     * Performs a POST or PUT request. Any data provided is set in the HTTP
     * client. String data is pushed in as raw POST data; array or object data
     * is pushed in as POST parameters.
     *
     * @param mixed $method
     * @param mixed $data
     * @return Zend_Http_Response
     */
    protected function _performPost($method, $data = null)
    {
        $client = self::getHttpClient();
        if (is_string($data)) {
            $client->setRawData($data, self::CONTENT_TYPE_APPLICATION_JSON);
        } elseif (is_array($data) || is_object($data)) {
            $client->setParameterPost((array) $data);
        }
        return $client->request($method);
    }
}