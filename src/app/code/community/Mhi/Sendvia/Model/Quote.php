<?php
/**
 * Created by PhpStorm.
 * User: jvaughan
 * Date: 25/09/2014
 * Time: 17:19
 */

/**
 * Class Mhi_Sendvia_Model_Quote
 *
 * @method string getReceiptId()
 * @method Mhi_Sendvia_Model_Quote setReceiptId(string $receiptData)
 *
 * @method string getReceiptData()
 * @method Mhi_Sendvia_Model_Quote setReceiptData(string $receiptData)
 *
 * @method string getQuoteData()
 * @method Mhi_Sendvia_Model_Quote setQuoteData(string $receiptData)
 *
 * @method integer getOrderId()
 * @method Mhi_Sendvia_Model_Quote setOrderId(integer $orderId)
 *
 * @method integer getStatus()
 * @method Mhi_Sendvia_Model_Quote setStatus(integer $status)
 *
 * @method string getCreatedAt()
 * @method Mhi_Sendvia_Model_Quote setCreatedAt(string $createdAt)
 */
class Mhi_Sendvia_Model_Quote extends Mage_Core_Model_Abstract
{

    const STATUS_UNBOOKED   = 0;
    const STATUS_BOOKED     = 10;
    const STATUS_PAID       = 20;
    const STATUS_INVALID    = 30;

    protected $_statuses = array(
        self::STATUS_UNBOOKED   => 'Unbooked',
        self::STATUS_BOOKED     => 'Booked',
        self::STATUS_PAID       => 'Paid',
        self::STATUS_INVALID    => 'Invalid',
    );

    protected function _construct()
    {
        $this->_init('mhi_sendvia/quote');
    }

    /**
     * @param integer $id
     * @return string
     */
    public function getStatusTextById($id)
    {
        return isset($this->_statuses[$id]) ? $this->_statuses[$id] : 'Unknown';
    }

    /**
     * @return array
     */
    public function getAllStatuses()
    {
        return $this->_statuses;
    }

    /**
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        return Mage::getModel('sales/order')->load($this->getOrderId());
    }

    /**
     * @return int
     */
    public function getStoreId() {
        return $this->getOrder()->getStoreId();
    }

    /**
     * @return $this
     */
    public function book()
    {
        $booking = Mage::getModel('mhi_sendvia/sendvia_booking')
            ->buildFromQuote($this)
            ->save();

        $this->setReceiptData(serialize($booking->getReceipt()));
        $this->setReceiptId($booking->getReceipt()->getId());
        $this->setStatus(self::STATUS_BOOKED);
        $this->save();

        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentUrl()
    {
        $url = $this->getSendviaReceipt()->getPaymentUrl();
        $url .= '?client_id=' . Mage::helper('mhi_sendvia/config')->getClientId($this->getStoreId());

        return $url;
    }

    /**
     * @return Mhi_Sendvia_Model_Sendvia_Quote
     */
    public function getSendviaQuote()
    {
        return unserialize($this->getQuoteData());
    }

    /**
     * @return Mhi_Sendvia_Model_Sendvia_Booking_Receipt
     */
    public function getSendviaReceipt()
    {
        return unserialize($this->getReceiptData());
    }

    /**
     * @return string|null
     */
    public function getBookingId()
    {
        $svReceipt = $this->getSendviaReceipt();
        return $svReceipt ? $svReceipt->getBookingId() : null;
    }

    /**
     * @return string|null
     */
    public function getShipmentId()
    {
        $receiptItems = $this->getSendviaReceipt()->getReceiptItems();
        if (count($receiptItems) == 1) {
            /**
             * @var Mhi_Sendvia_Model_Sendvia_Object $receiptItem
             */
            $receiptItem = $receiptItems[0];
            return $receiptItem->getShipmentId();
        }

        return null;
    }

    public function setStatusToPaid() {
        $this->setStatus(self::STATUS_PAID);
    }

    public function isStatusInvalid() {
        return $this->getStatus() == self::STATUS_INVALID;
    }

    /**
     * @return string|mixed
     */
    public function getShippingLabelPdf()
    {

        Mage::helper('mhi_sendvia/debug')->debug($this->getSendviaReceipt()->getData());
        $receiptItems = $this->getSendviaReceipt()->getReceiptItems();

        if (count($receiptItems) != 1) {
            Mage::throwException('Expected 1 receipt item');
        }

        /**
         * @var Mhi_Sendvia_Model_Sendvia_Object
         */
        $receiptItem = $receiptItems[0];

        $pdf = Mage::getModel('mhi_sendvia/sendvia_shipment')
            ->setId($receiptItem->getShipmentId())
            ->getShippingLabelPdf()
        ;

        return $pdf;
    }

    protected function _beforeSave()
    {
        if (! $this->getCreatedAt()) {
            $this->setCreatedAt(
                Mage::app()->getLocale()->storeDate(null, Varien_Date::now(), true)
            );
        }

        parent::_beforeSave();
    }

}