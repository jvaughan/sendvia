<?php

abstract class Mhi_Sendvia_Test_Case extends EcomDev_PHPUnit_Test_Case {

    public function setUp()
    {
        parent::setUp();

        $mockDebugHelper = $this->getMockBuilder('Mhi_Sendvia_Helper_Debug')
            ->setMethods(array('isDebugEnabled'))
            ->getMock()
        ;
        $mockDebugHelper->expects($this->any())
            ->method('isDebugEnabled')
            ->will($this->returnValue(false))
        ;
        $this->replaceByMock(
            'helper',
            'mhi_sendvia/debug',
            $mockDebugHelper
        );


        $mockRestClient = $this->getMockBuilder('Mhi_Sendvia_Model_Rest_Client')
            ->disableOriginalConstructor()
            ->setMethods(array('restGet', 'restPost', 'restPut', 'restDelete'))
            ->getMock()
        ;
        $mockRestClient->expects($this->any())
            ->method('restGet')
            ->will($this->returnValue(false))
        ;
        $mockRestClient->expects($this->any())
            ->method('restPost')
            ->will($this->returnValue(false))
        ;
        $mockRestClient->expects($this->any())
            ->method('restPut')
            ->will($this->returnValue(false))
        ;
        $mockRestClient->expects($this->any())
            ->method('restDelete')
            ->will($this->returnValue(false))
        ;
        $this->replaceByMock(
            'model',
            'mhi_sendvia/rest_client',
            $mockRestClient
        );
    }

    /**
     * @param string $string
     * @param string $message
     */
    public function assertIsValidGuid($string, $message = '')
    {
        $this->assertTrue(
            !empty($string) && preg_match('/^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/', $string),
            $message
        );
    }
}