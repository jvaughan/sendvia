<?php

class Mhi_Sendvia_Test_Helper_Locale extends Mhi_Sendvia_Test_Case {

    /**
     * @var Mhi_Sendvia_Helper_Locale
     */
    protected $_helper;

    public function setUp()
    {
        parent::setUp();

        $this->_helper = Mage::helper('mhi_sendvia/locale');
    }

    public function tearDown()
    {
        unset($this->_helper);
        parent::tearDown();
    }

    /**
     * @test
     * @dataProvider twoCharProvider
     * @param string $alpha
     * @param integer $expectedCode
     */
    public function testGetNumericCcFrom2LetterAlpha($alpha, $expectedCode)
    {
        $this->assertEquals(
            $expectedCode,
            $this->_helper->getNumericCcFrom2LetterAlpha($alpha)
        );
    }

    /**
     * @test
     * @dataProvider threeCharProvider
     * @param string $alpha
     * @param integer $expectedCode
     */
    public function testGetCurrencyCodeFrom3LetterAlpha($alpha, $expectedCode)
    {
        $this->assertEquals(
            $expectedCode,
            $this->_helper->getCurrencyCodeFrom3LetterAlpha($alpha)
        );
    }

    public function twoCharProvider()
    {
        return array(
            array('US', 840),
            array('GB', 826),
            array('XX', null)
        );
    }

    public function threeCharProvider()
    {
        return array(
            array('USD', 840),
            array('GBP', 826),
            array('none', null)
        );
    }

}
