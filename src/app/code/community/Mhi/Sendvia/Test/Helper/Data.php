<?php

class Mhi_Sendvia_Test_Helper_Data extends Mhi_Sendvia_Test_Case {

    /**
     * @var Mhi_Sendvia_Helper_Data
     */
    protected $_helper;

    public function setUp()
    {
        parent::setUp();

        $this->_helper = Mage::helper('mhi_sendvia');
    }

    public function tearDown()
    {
        unset($this->_helper);
        parent::tearDown();
    }

    /**
     * @test
     */
    public function testGetRedirectUrl()
    {
        $mockSession = $this->getMockBuilder('Mage_Core_Model_Session')
            ->disableOriginalConstructor()
            ->getMock()
        ;
        $this->replaceByMock(
            'singleton',
            'core/session',
            $mockSession
        );

        $baseUrl = Mage::getUrl();

        $this->assertEquals(
            $baseUrl . 'sendvia/paymentreturn/',
            $this->_helper->getRedirectUrl()
        );
    }
}