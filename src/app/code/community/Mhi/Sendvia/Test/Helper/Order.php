<?php

class Mhi_Sendvia_Test_Helper_Order extends Mhi_Sendvia_Test_Case {

    const METHOD_STRING = 'mhi_sendvia_abcdefgh-1234-abcd-1234-abcdefghijkl';

    /**
     * @var Mhi_Sendvia_Helper_Order
     */
    protected $_helper;

    public function setUp()
    {
        parent::setUp();

        $this->_helper = Mage::helper('mhi_sendvia/order');
    }

    public function tearDown() {
        unset($this->_helper);
        parent::tearDown();
    }

    /**
     * @test
     */
    public function testIsSendviaOrderWhenTrue()
    {
        $order = Mage::getModel('sales/order')->setShippingMethod(
            self::METHOD_STRING
        );

        $this->assertTrue(
            $this->_helper->isSendviaOrder($order)
        );
    }

    /**
     * @test
     */
    public function testIsSendviaOrderWhenFalse()
    {
        $order = Mage::getModel('sales/order')->setShippingMethod(
            'non_sendvia_method'
        );

        $this->assertFalse(
            $this->_helper->isSendviaOrder($order)
        );
    }

    /**
     * @test
     */
    public function testGetSendviaServiceId()
    {
        $order = Mage::getModel('sales/order')->setShippingMethod(
            self::METHOD_STRING
        );

        $this->assertEquals(
            'abcdefgh-1234-abcd-1234-abcdefghijkl',
            $this->_helper->getSendviaServiceId($order)
        );
    }

    /**
     * @test
     */
    public function testGetSendviaQuoteId()
    {
        $order = Mage::getModel('sales/order')
            ->setShippingMethod(self::METHOD_STRING)
            ->setMhiSendviaQuoteId('quote-id')
        ;

        $this->assertEquals(
            'quote-id',
            $this->_helper->getSendviaQuoteId($order)
        );
    }

    /**
     * @test
     */
    public function testGetSendviaServiceIsNullWhenNotSvMethod()
    {
        $order = Mage::getModel('sales/order')->setShippingMethod(
            'not_sendvia_method'
        );

        $this->assertNull(
            $this->_helper->getSendviaServiceId($order)
        );
    }


    /**
     * @test
     */
    public function testGetSendviaQuoteIdIsNullWhenNotSvMethod()
    {
        $order = Mage::getModel('sales/order')
            ->setShippingMethod('not-sendvia-method')
            ->setMhiSendviaQuoteId('quote-id')
        ;

        $this->assertNull(
            $this->_helper->getSendviaQuoteId($order)
        );
    }

    /**
     * @test
     */
    public function testReQuoteCallsWebserviceWithPreviousQueryId() {
        $order = Mage::getModel('sales/order')
            ->setMhiSendviaQueryId('query-id');

        $this->replaceByMock(
            'model',
            'mhi_sendvia/webservice_request',
            $this->_getMockWebserviceRequestModelForCreate()
        );

        $this->_helper->reQuote($order);
    }


    /**
     * @test
     * @loadFixture
     */
    public function testShouldAllowRequoteIsTrueIfNoQuotesExist()
    {
        $order = Mage::getModel('sales/order')->load(1);
        $this->assertTrue(
            $this->_helper->shouldAllowRequote($order)
        );
    }

    /**
     * @test
     * @loadFixture
     */
    public function testShouldAllowRequoteIsTrueIfAllQuotesAreInvalid()
    {
        $order = Mage::getModel('sales/order')->load(1);
        $this->assertTrue(
            $this->_helper->shouldAllowRequote($order)
        );
    }

    /**
     * @test
     * @loadFixture
     */
    public function testShouldNotAllowRequoteIfValidQuotesArePresent()
    {
        $order = Mage::getModel('sales/order')->load(1);
        $this->assertFalse(
            $this->_helper->shouldAllowRequote($order)
        );
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function _getMockWebserviceRequestModelForCreate()
    {
        $mock = $this->getMockBuilder('Mhi_Sendvia_Model_Webservice_Request')
            ->disableOriginalConstructor()
            ->setMethods(array('create'))
            ->getMock()
        ;

        $mock->expects($this->once())
            ->method('create')
            ->with('quotes', array('id' => 'query-id'))
            ->will($this->returnValue($this->_getMockResponseModel()))
        ;

        return $mock;
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function _getMockResponseModel()
    {
        $mock = $this->getMockBuilder('Mhi_Sendvia_Model_Webservice_Response')
            ->disableOriginalConstructor()
            ->setMethods(array('getItem'))
            ->getMock()
        ;

        $mock->expects($this->any())
            ->method('getItem')
            ->will($this->returnValue(
                Mage::getModel('mhi_sendvia/sendvia_object')->setData(array(
                    'id'    => 'new-query-id'
                ))
            ));

        return $mock;
    }
}