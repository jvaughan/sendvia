<?php

class Mhi_Sendvia_Test_Helper_Webservice extends Mhi_Sendvia_Test_Case {

    /**
     * @var Mhi_Sendvia_Helper_Webservice
     */
    protected $_helper;

    /**
     * @var Varien_Object
     */
    protected $_nestedVarienObjects;

    /**
     * @var array
     */
    protected $_nestedVarienObjectsExpectedResult;

    public function setUp()
    {
        parent::setUp();
        $this->_helper = Mage::helper('mhi_sendvia/webservice');

        $this->_nestedVarienObjects = new Varien_Object(array(
            'field1'        => 'value1',
            'field2'        => 'value2',
            'secondObject'  => new Varien_Object(array(
                'nestedField1'  => 'nested_value1',
                'nestedField2'  => 'nested_value2',
                'thirdObject'   => new Varien_Object(array(
                    'nested2Field1' => 'nested2_value1',
                 ))
            )),
        ));

        $this->_nestedVarienObjectsExpectedResult = array(
            'Field1'        => 'value1',
            'Field2'        => 'value2',
            'SecondObject'  => array(
                'NestedField1'  => 'nested_value1',
                'NestedField2'  => 'nested_value2',
                'ThirdObject'   => array(
                    'Nested2Field1' => 'nested2_value1',
                )
            ),
        );
    }

    public function tearDown()
    {
        unset($this->_helper);
        parent::tearDown();
    }

    /**
     * @test
     */
    public function testRestClientIsInstanceOfSendviaRestClient()
    {
        $this->assertInstanceOf(
            'Zend_Rest_Client',
            $this->_helper->getRestClient(),
            'getRestClient() did not return Zend_Rest_Client'
        );

        $this->assertInstanceOf(
            'Mhi_Sendvia_Model_Rest_Client',
            $this->_helper->getRestClient(),
            'getRestClient() did not return Mhi_Sendvia_Model_Rest_Client'
        );
    }

    /**
     * @test
     */
    public function testRestClientHasCorrectUriSet()
    {
        $restClient = $this->_helper->getRestClient();

        $this->assertEquals(
            'https://www.sendvia.co.uk/rest/alpha4',
            (string) $restClient->getUri()
        );
    }

    /**
     * @test
     */
    public function testGuidIsValid()
    {
        $guid = $this->_helper->generateGuid();

        $this->assertIsValidGuid(
            $guid,
            "GUID $guid is not valid"
        );
    }

    /**
     * @test
     */
    public function testPrepareDataReturnsNestedArraysWithUcfirstKeysWhenGivenVarienObjects()
    {
        $expected = array(
            'Field1'        => 'value1',
            'Field2'        => 'value2',
            'SecondObject'  => array(
                'NestedField1' => 'nested_value1',
                'NestedField2' => 'nested_value2',
                'ThirdObject'   => array(
                    'Nested2Field1'    => 'nested2_value1',
                )
            ),
        );

        $returned = $this->_helper->prepareData($this->_nestedVarienObjects);

        $this->assertInternalType(
            'array',
            $returned
        );

        $this->assertArrayHasKey(
            'SecondObject',
            $returned
        );

        $this->assertInternalType(
            'array',
            $returned['SecondObject']
        );

        $this->assertEquals(
            $expected,
            $returned
        );
    }

    /**
     * @test
     */
    public function testPrepareDataReturnsUcfirstArrayWhenPassedNestedArray()
    {
        $data = array(
            'field1'        => 'value1',
            'field2'        => 'value2',
            'secondArray'   => array(
                'nestedField1' => 'nested_value1',
                'nestedField2' => 'nested_value2',
            ),
        );

        $expected = array(
            'Field1'        => 'value1',
            'Field2'        => 'value2',
            'SecondArray'   => array(
                'NestedField1' => 'nested_value1',
                'NestedField2' => 'nested_value2',
            ),
        );

        $returned = $this->_helper->prepareData($data);

        $this->assertInternalType(
            'array',
            $returned
        );

        $this->assertInternalType(
            'array',
            $returned['SecondArray']
        );

        $this->assertEquals(
            $expected,
            $returned
        );
    }

    /**
     * @test
     */
    public function testPrepareDataAsJsonReturnsValidJson()
    {
        $expected = '{"Field1":"value1","Field2":"value2","SecondObject":{"NestedField1":"nested_value1","NestedField2":"nested_value2","ThirdObject":{"Nested2Field1":"nested2_value1"}}}';
        $returned = $this->_helper->prepareDataAsJson($this->_nestedVarienObjects);

        $this->assertInternalType('string', $returned);
        $this->assertEquals($expected, $returned);

        $this->assertEquals(
            $this->_nestedVarienObjectsExpectedResult,
            json_decode($returned, true)
        );
    }

    /**
     * @test
     */
    public function testPrepareDataAsJsonReturnsValidJsonWhenContainerIsArray()
    {
        $expected = '{"Field1":"value1","Field2":"value2","SecondObject":{"NestedField1":"nested_value1","NestedField2":"nested_value2","ThirdObjectLongName":{"Nested2Field1":"nested2_value1"}}}';

        $data = array(
            'field1'        => 'value1',
            'field2'        => 'value2',
            'secondObject'  => new Varien_Object(array(
                'nested_field1'  => 'nested_value1',
                'nestedField2'  => 'nested_value2',
                'third_object_long_name'   => new Varien_Object(array(
                    'nested2Field1' => 'nested2_value1',
                ))
            )),
        );

        $returned = $this->_helper->prepareDataAsJson($data);

        $this->assertEquals(
            $expected,
            $returned
        );
    }
}