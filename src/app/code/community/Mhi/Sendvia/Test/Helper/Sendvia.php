<?php

class Mhi_Sendvia_Test_Helper_Sendvia extends Mhi_Sendvia_Test_Case {

    /**
     * @var Mhi_Sendvia_Helper_Sendvia
     */
    protected $_helper;

    protected $_serviceCollections = array();

    public function setUp()
    {
        parent::setUp();

        $this->_helper = Mage::helper('mhi_sendvia/sendvia');
        $this->_replaceCarrierModelWithMock();
        $this->_replaceServiceModelWithMock();
    }

    public function tearDown()
    {
        unset($this->_helper);
        parent::tearDown();
    }

    /**
     * @test
     * @loadFixture
     */
    public function testUpdateLocalCarriersAndServices()
    {
        $this->assertCount(
            0,
            Mage::getModel('mhi_sendvia/sendvia_local_carrier')->getCollection()
        );

        for ($i = 0; $i <= 1; $i++) {
            $this->_helper->updateLocalCarriersAndServices();

            $this->_assertCollectionDataEquals(
                array(
                    array(
                        'sendvia_id'    => 'sv-carrier-id-1',
                        'name'          => 'carrier 1'
                    ),
                    array(
                        'sendvia_id'    => 'sv-carrier-id-2',
                        'name'          => 'carrier 2'
                    )
                ),
                Mage::getModel('mhi_sendvia/sendvia_local_carrier')->getCollection()->load(),
                array('carrier_id'),
                'local carrier collection does not match expectation'
            );

            $this->_assertCollectionDataEquals(
                array(
                    array(
                        'sendvia_id'    => 'sv-service-id-1',
                        'name'          => 'service 1 (carrier 1)'
                    ),
                    array(
                        'sendvia_id'    => 'sv-service-id-2',
                        'name'          => 'service 2 (carrier 1)'
                    ),
                    array(
                        'sendvia_id'    => 'sv-service-id-3',
                        'name'          => 'service 3 (carrier 2)'
                    ),
                    array(
                        'sendvia_id'    => 'sv-service-id-4',
                        'name'          => 'service 4 (carrier 2)'
                    )

                ),
                Mage::getModel('mhi_sendvia/sendvia_local_carrier_service')->getCollection()->load(),
                array('service_id', 'carrier_id'),
                'local service collection does not match expectation'
            );
        }
    }

    /**
     * @test
     * @loadFixture
     */
    public function testUpdateLocalCarriersAndServicesDoesNotUpdateTablesWhenNoChanges()
    {

        $carrierCollection = Mage::getModel('mhi_sendvia/sendvia_local_carrier')->getCollection()->load();
        $serviceCollection = Mage::getModel('mhi_sendvia/sendvia_local_carrier_service')->getCollection()->load();

        $origCarrierData = array();
        foreach ($carrierCollection as $carrier)  {
            $origCarrierData[] = $carrier->getData();
        }
        $origServiceData = array();
        foreach ($serviceCollection as $service)  {
            $origServiceData[] = $service->getData();
        }

        $this->_helper->updateLocalCarriersAndServices();

        $newCarrierData = array();
        foreach (Mage::getModel('mhi_sendvia/sendvia_local_carrier')->getCollection()->load() as $carrier)  {
            $newCarrierData[] = $carrier->getData();
        }
        $newServiceData = array();
        foreach (Mage::getModel('mhi_sendvia/sendvia_local_carrier_service')->getCollection()->load() as $service)  {
            $newServiceData[] = $service->getData();
        }

        $this->assertEquals(
            $origCarrierData,
            $newCarrierData,
            'new carrier data does not match original'
        );

        $this->assertEquals(
            $origServiceData,
            $newServiceData,
            'new service data does not match original'
        );
    }

    /**
     * @param array $expected
     * @param Varien_Data_Collection $collection
     * @param array $ignoreFields
     */
    protected function _assertCollectionDataEquals(
        $expected = array(),
        Varien_Data_Collection $collection,
        $ignoreFields = array(),
        $message = ''
    )
    {
        $collectionData = array();
        foreach ($collection->getItems() as $cItem) {
            $data = $cItem->getData();
            foreach ($ignoreFields as $ignoreField) {
                unset($data[$ignoreField]);
            }
            $collectionData[] = $data;
        }

        $this->assertEquals(
            $expected,
            $collectionData,
            $message
        );
    }

    protected function _replaceCarrierModelWithMock()
    {
        $carrierCollection = Mage::getModel('mhi_sendvia/sendvia_collection');
        $carrierCollection->addItem(Mage::getModel('mhi_sendvia/sendvia_carrier')->setData(array(
            'id'    => 'sv-carrier-id-1',
            'name'  => 'carrier 1',
        )));
        $carrierCollection->addItem(Mage::getModel('mhi_sendvia/sendvia_carrier')->setData(array(
            'id'    => 'sv-carrier-id-2',
            'name'  => 'carrier 2',
        )));

        $mockCarrier = $this->getMockBuilder('Mhi_Sendvia_Model_Sendvia_Carrier')
            ->setMethods(array('getCollection'))
            ->getMock()
        ;

        $mockCarrier->expects($this->any())
            ->method('getCollection')
            ->will($this->returnValue(
                $carrierCollection
            ));

        $this->replaceByMock(
            'model',
            'mhi_sendvia/sendvia_carrier',
            $mockCarrier
        );
    }

    protected function _replaceServiceModelWithMock()
    {
        $this->_serviceCollections = array(
            'sv-carrier-id-1' => Mage::getModel('mhi_sendvia/sendvia_collection')
                    ->addItem(Mage::getModel('mhi_sendvia/sendvia_carrier_service')->setData(array(
                        'id'    => 'sv-service-id-1',
                        'name'  => 'service 1 (carrier 1)',
                    )))
                    ->addItem(Mage::getModel('mhi_sendvia/sendvia_carrier_service')->setData(array(
                        'id'    => 'sv-service-id-2',
                        'name'  => 'service 2 (carrier 1)',
                    )))
            ,
            'sv-carrier-id-2' => Mage::getModel('mhi_sendvia/sendvia_collection')
                    ->addItem(Mage::getModel('mhi_sendvia/sendvia_carrier_service')->setData(array(
                        'id'    => 'sv-service-id-3',
                        'name'  => 'service 3 (carrier 2)',
                    )))
                    ->addItem(Mage::getModel('mhi_sendvia/sendvia_carrier_service')->setData(array(
                        'id'    => 'sv-service-id-4',
                        'name'  => 'service 4 (carrier 2)',
                    )))
        );


        $mockService = $this->getMockBuilder('Mhi_Sendvia_Model_Sendvia_Carrier_Service')
            ->setMethods(array('getCollection'))
            ->getMock()
        ;
        $mockService->expects($this->any())
            ->method('getCollection')
            ->will(
                $this->returnCallback(
                    function () use (&$mockService) {
                        return $this->_serviceCollections[ $mockService->getParentId('carrier') ];
                    }
                )
            )
        ;

        $this->replaceByMock(
            'model',
            'mhi_sendvia/sendvia_carrier_service',
            $mockService
        );
    }
}
