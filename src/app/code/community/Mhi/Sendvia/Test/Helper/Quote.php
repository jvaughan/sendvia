<?php

class Mhi_Sendvia_Test_Helper_Quote extends Mhi_Sendvia_Test_Case {
    /**
     * @var Mhi_Sendvia_Helper_Quote
     */
    protected $_helper;

    public function setUp()
    {
        parent::setUp();

        $this->_helper = Mage::helper('mhi_sendvia/quote');
        Mage::unregister('mhi_sendvia_last_quote_id');
    }

    public function tearDown() {
        unset($this->_helper);
        Mage::unregister('mhi_sendvia_last_quote_id');
        parent::tearDown();
    }

    /**
     * @test
     */
    public function testGetLastQuoteIdIsNullWhenNotSet()
    {
        $this->assertNull(
            $this->_helper->getLastQuoteId(),
            'getLastQuoteId() should return null when it hasn\'t been set'
        );
    }

    /**
     * @test
     */
    public function testGetLastQuoteId()
    {
        $quoteId = 'quote-id';

        $this->_helper->registerQuoteId($quoteId);

        $this->assertEquals(
            $quoteId,
            $this->_helper->getLastQuoteId()
        );
    }

}