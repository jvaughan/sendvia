<?php

class Mhi_Sendvia_Test_Helper_Config extends Mhi_Sendvia_Test_Case {

    /**
     * @var Mhi_Sendvia_Helper_Config
     */
    protected $_helper;

    public function setUp()
    {
        parent::setUp();

        $this->_helper = Mage::helper('mhi_sendvia/config');
    }

    public function tearDown() {
        unset($this->_helper);
        parent::tearDown();
    }

    /**
     * @test
     */
    public function testGetWebserviceUri()
    {
        $this->assertEquals(
            'https://www.sendvia.co.uk/rest/alpha4',
            $this->_helper->getWebserviceUri()
        );
    }

    /**
     * @test
     */
    public function testGetAuthUri()
    {
        $this->assertEquals(
            'https://www.sendvia.co.uk/alpha4/token',
            $this->_helper->getAuthUri()
        );
    }

    /**
     * @test
     */
    public function testGetWebServiceTimeout()
    {
        $this->assertEquals(
            '30',
            $this->_helper->getWebserviceTimeout()
        );
    }

    /**
     * @test
     * @loadFixture
     */
    public function testIsCarrierEnabled() {
        $this->assertTrue(
            $this->_helper->isCarrierEnabled('enabled-id')
        );

        $this->assertFalse(
            $this->_helper->isCarrierEnabled('disabled-id')
        );

        $this->assertFalse(
            $this->_helper->isCarrierEnabled('non-present-id')
        );
    }

    /**
     * @test
     * @loadFixture
     */
    public function testIsServiceEnabled()
    {
        $this->assertTrue(
            $this->_helper->isServiceEnabled('carrier-id', 'enabled-service-id')
        );

        $this->assertFalse(
            $this->_helper->isServiceEnabled('carrier-id', 'disabled-service-id')
        );

        $this->assertFalse(
            $this->_helper->isServiceEnabled('non-present-carrier-id', 'non-present-service-id')
        );

    }

    /**
     * @test
     * @loadFixture
     */
    public function testIsCarrierRestrictedToSpecificCountries()
    {
        $this->assertTrue(
            $this->_helper->isCarrierRestrictedToSpecificCountries('restricted-carrier-id')
        );

        $this->assertFalse(
            $this->_helper->isCarrierRestrictedToSpecificCountries('non-restricted-carrier-id')
        );

        $this->assertFalse(
            $this->_helper->isCarrierRestrictedToSpecificCountries('non-present-carrier-id')
        );
    }

    /**
     * @test
     * @loadFixture
     */
    public function testGetCarrierAllowedCountries()
    {
        $this->assertEquals(
            $this->_helper->getCarrierAllowedCountries('gb-us-carrier-id'),
            array('US', 'GB')
        );

        $this->assertEmpty(
            $this->_helper->getCarrierAllowedCountries('no-countries-carrier-id')
        );

        $this->assertEquals(
            $this->_helper->getCarrierAllowedCountries('non-present-carrier-id'),
            array()
        );
    }


}