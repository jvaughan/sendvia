<?php

class Mhi_Sendvia_Test_Model_Sendvia_Booking extends Mhi_Sendvia_Test_Case {

    /**
     * @var Mhi_Sendvia_Model_Sendvia_Booking
     */
    protected $_model;

    const GENERATED_GUID    = 'generated-guid';

    protected $_receiptData = array(
        'field1'    => 'value1',
        'field2'    => 'value2',
    );

    public function setUp()
    {
        parent::setUp();
        $this->_model = Mage::getModel('mhi_sendvia/sendvia_booking');

        $mockWsHelper = $this->getMockBuilder('Mhi_SendVia_Helper_Webservice')
            ->setMethods(array('generateGuid'))
            ->getMock()
        ;
        $mockWsHelper->expects($this->any())
            ->method('generateGuid')
            ->will($this->returnValue(self::GENERATED_GUID))
        ;
        $this->replaceByMock(
            'helper',
            'mhi_sendvia/webservice',
            $mockWsHelper
        );
    }

    public function tearDown()
    {
        unset($this->_model);
        parent::tearDown();
    }

    /**
     * @test
     */
    public function testSavePopulatesReceiptWithReturnedData()
    {
        $this->replaceByMock(
            'model',
            'mhi_sendvia/webservice_request',
            $this->_getMockWebserviceRequestModelForCreate()
        );

        $m = $this->_model;

        $expectedReceipt = Mage::getModel('mhi_sendvia/sendvia_booking_receipt')->setData(
            $this->_receiptData
        );

        $m->save();
        $this->assertEquals(
            $m->getData(),
            array('id'  => self::GENERATED_GUID),
            'Booking model should only contain newly generated id'
        );

        $this->assertEquals(
            $expectedReceipt,
            $m->getReceipt(),
            'Receipt data does not match expectation'
        );
    }

    /**
     * @test
     */
    public function testLoadPopulatesReceiptWithReturnedData()
    {
        $this->replaceByMock(
            'model',
            'mhi_sendvia/webservice_request',
            $this->_getMockWebserviceRequestModelForRead()
        );

        $m = $this->_model;

        $expectedReceipt = Mage::getModel('mhi_sendvia/sendvia_booking_receipt')->setData(
            $this->_receiptData
        );

        $m->load(self::GENERATED_GUID);
        $this->assertEquals(
            $m->getData(),
            array(),
            'Booking model should have no data'
        );

        $this->assertEquals(
            $expectedReceipt,
            $m->getReceipt(),
            'Receipt data does not match expectation'
        );
    }

    /**
     * @test
     */
    public function testBuildFromOrderPopulatesQuoteIdAndPaymentMethod()
    {
        $m = $this->_model;

        $mockCfgHelper = $this->getMockBuilder('Mhi_Sendvia_Helper_Config')
            ->disableOriginalConstructor()
            ->setMethods(array('getPaymentMethod'))
            ->getMock()
        ;

        $mockCfgHelper->expects($this->once())
            ->method('getPaymentMethod')
            ->will($this->returnValue(99))
        ;

        $this->replaceByMock(
            'helper',
            'mhi_sendvia/config',
            $mockCfgHelper
        );


        $order = Mage::getModel('sales/order')
            ->setShippingMethod('mhi_sendvia_methodid-1234-1234-1234-123456789012')
            ->setMhiSendviaQuoteId('quoteid1-1234-1234-1234-123456789012')
            ->setGlobalCurrencyCode('GBP')
        ;

        $this->assertFalse(
            $m->hasData()
        );

        $expectedModel =  Mage::getModel('mhi_sendvia/sendvia_booking')
            ->setQuoteId('quoteid1-1234-1234-1234-123456789012')
            ->setPaymentMethod(99)
            ->setCurrency(826)
        ;

        $m->buildFromOrder($order);

        $this->assertEquals(
           $expectedModel,
            $m,
            'Booking model does not contain expected data'
        );
    }

    /**
     * @test
     * @loadFixture
     */
    public function testBuildFromQuote()
    {
        $expBookingData = array(
            'quote_id'          => 'sendvia-quote-id',
            'payment_method'    => 1,
            'currency'          => 826,
            'booking_shipments' => array(
                Mage::getModel('mhi_sendvia/sendvia_object')->setData(array(
                    'service_id'    => 'service1-1234-1234-1234-123456789012',
                    'shipment'      => Mage::getModel('mhi_sendvia/sendvia_object')->setData(array(
                            'id'        => 'shipment-id',
                        ))
                ))
            )
        );

        $this->assertFalse(
            $this->_model->hasData()
        );

        $svQuote = Mage::getModel('mhi_sendvia/sendvia_quote')->setQuoteShipments(array(
            Mage::getModel('mhi_sendvia/sendvia_object')->setData(array(
                'shipment_id'   => 'shipment-id',
                'service'       => Mage::getModel('mhi_sendvia/sendvia_object')->setData(array(
                    'id'            => 'service1-1234-1234-1234-123456789012',
                )),
            )),
        ));

        $mockQuote = $this->getMockBuilder('Mhi_Sendvia_Model_Quote')
            ->setMethods(array('getSendviaQuote'))
            ->getMock()
        ;
        $mockQuote->expects($this->once())
            ->method('getSendviaQuote')
            ->will($this->returnValue($svQuote))
        ;
        $mockQuote->load(1);

        $this->_model->buildFromQuote($mockQuote);

        $this->assertEquals(
            $expBookingData,
            $this->_model->getData()
        );
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function _getMockWebserviceRequestModelForCreate()
    {
        $mock = $this->getMockBuilder('Mhi_Sendvia_Model_Webservice_Request')
            ->disableOriginalConstructor()
            ->setMethods(array('create'))
            ->getMock()
        ;

        $mock->expects($this->once())
            ->method('create')
            ->with('bookings', array('id' => self::GENERATED_GUID))
            ->will($this->returnValue($this->_getMockResponseModel()))
        ;

        return $mock;
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function _getMockWebserviceRequestModelForRead()
    {
        $mock = $this->getMockBuilder('Mhi_Sendvia_Model_Webservice_Request')
            ->disableOriginalConstructor()
            ->setMethods(array('read'))
            ->getMock()
        ;

        $mock->expects($this->once())
            ->method('read')
            ->with('bookings/' . self::GENERATED_GUID)
            ->will($this->returnValue($this->_getMockResponseModel()))
        ;

        return $mock;
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function _getMockResponseModel()
    {
        $mock = $this->getMockBuilder('Mhi_Sendvia_Model_Webservice_Response')
            ->disableOriginalConstructor()
            ->setMethods(array('getItem'))
            ->getMock()
        ;

        $mock->expects($this->any())
            ->method('getItem')
            ->will($this->returnValue(
                Mage::getModel('mhi_sendvia/sendvia_object', $this->_receiptData)
            ));

        return $mock;
    }

}