<?php

class Mhi_Sendvia_Test_Model_Webservice_Response extends Mhi_Sendvia_Test_Case {

    /**
     * @var Mhi_Sendvia_Model_Webservice_Response
     */
    protected $_model;

    protected $_multipleCarrierJson = '[{"Id":"bb86e72b-ccf7-422c-9e3a-023196cd476a","Name":"New Carrier","Logo":"AA==","Private":true},{"Id":"3973dff5-4b0d-45a4-bd0a-04e5a2640da4","Name":"New Carrier","Logo":"AA==","Private":true},{"Id":"00ed6872-68bc-47eb-8a46-0b644fa06a2c","Name":"New Carrier","Logo":"AA==","Private":true},{"Id":"9b288159-4104-403e-82e5-0bb9c782ecf4","Name":"New Carrier","Logo":"AA==","Private":true},{"Id":"94f457a9-ffa8-431e-b378-0c8a36fc2a80","Name":"New Carrier","Logo":"AA==","Private":true}]';

    public function setUp()
    {
        parent::setUp();
        $this->_model = Mage::getModel('mhi_sendvia/webservice_response');
    }

    public function tearDown()
    {
        unset($this->_model);
        parent::tearDown();
    }

    /**
     * @test
     */
    public function testGetItemWhenSingleCarrierReturned()
    {
        $m = $this->_model;

        $m->setHttpResponse(
            $this->_getMockHttpResponse('{"Id":"bb86e72b-ccf7-422c-9e3a-023196cd476a","Name":"New Carrier","Logo":"AA==","Private":true}')
        );

        $item = $m->getItem();
        $this->assertInstanceOf(
            'Varien_Object',
            $item,
            'Response Item should descend from Varien_Object'
        );
        $this->assertInstanceOf(
            'Mhi_Sendvia_Model_Sendvia_Object',
            $item,
            'Response Item should be Mhi_Sendvia_Model_Sendvia_Object'
        );
        $this->assertTrue($item->hasData(), 'Object model should have data');

        $this->assertEquals(
            array(
                'id'        => 'bb86e72b-ccf7-422c-9e3a-023196cd476a',
                'name'      => 'New Carrier',
                'logo'      => 'AA==',
                'private'   =>  true,
            ),
            $item->getData()
        );
    }

    /**
     * @test
     */
    public function testGetItemReturnsOnlyFirstItemWhenMultipleCarriersReturned()
    {
        $m = $this->_model;

        //print_r(json_decode($this->_multipleCarrierJson), true);exit;

        $m->setHttpResponse(
            $this->_getMockHttpResponse($this->_multipleCarrierJson)
        );

        $item = $m->getItem();
        $this->assertInstanceOf(
            'Varien_Object',
            $item,
            'Response Item should descend from Varien_Object'
        );
        $this->assertInstanceOf(
            'Mhi_Sendvia_Model_Sendvia_Object',
            $item,
            'Response Item should be Mhi_Sendvia_Model_Sendvia_Object'
        );
        $this->assertTrue($item->hasData(), 'Object model should have data');

        $this->assertEquals(
            array(
                'id'        => 'bb86e72b-ccf7-422c-9e3a-023196cd476a',
                'name'      => 'New Carrier',
                'logo'      => 'AA==',
                'private'   =>  true,
            ),
            $item->getData()
        );
    }

    /**
     * @test
     */
    public function testGetAllItemsReturnsCorrectArrayOfSendviaObjects()
    {
        $m = $this->_model;

        $m->setHttpResponse(
            $this->_getMockHttpResponse($this->_multipleCarrierJson)
        );

        $items = $m->getAllItems();
        $this->assertInternalType('array', $items);
        $this->assertCount(5, $items);
        foreach ($items as $item) {
            $this->assertInstanceOf(
                'Varien_Object',
                $item,
                'Response Item should descend from Varien_Object'
            );
            $this->assertInstanceOf(
                'Mhi_Sendvia_Model_Sendvia_Object',
                $item,
                'Response Item should be Mhi_Sendvia_Model_Sendvia_Object'
            );
        }

        $testItem = $items[2];
        $this->assertEquals(
            array(
                'id'        => '00ed6872-68bc-47eb-8a46-0b644fa06a2c',
                'name'      => 'New Carrier',
                'logo'      => 'AA==',
                'private'   => true,
            ),
            $testItem->getData()
        );

    }

    /**
     * @test
     */
    public function testGetAllItemsReturnsCorrectArrayWhenResponseIsCollectionContainingOneItem()
    {
        $m = $this->_model;

        $json = '[{"Id":"15320ba1-7579-4cb4-9e9c-f6ed86fd6945","Name":"Hermes Discounted","Logo":"AAAAAA==","Private":false}]';

        $m->setHttpResponse(
            $this->_getMockHttpResponse($json)
        );

        $items = $m->getAllItems();

        $this->assertCount(
            1,
            $items
        );

        $this->assertEquals(
            array(
                'id'        => '15320ba1-7579-4cb4-9e9c-f6ed86fd6945',
                'name'      => 'Hermes Discounted',
                'logo'      => 'AAAAAA==',
                'private'   => false,
            ),
            $items[0]->getData()
        );
    }

    /**
     * @test
     */
    public function testGetItemRecursivelyConvertsJsonObjectsToSendviaObjects()
    {
        $json = '[{
            "Name":"My name",
            "ArrayOfObjects":[
                {"Name":"Obj 1 name"}
            ]
            }]';

        $expected = Mage::getModel('mhi_sendvia/sendvia_object')->setData(array(
            'name'              => 'My name',
            'array_of_objects'    => array(
                Mage::getModel('mhi_sendvia/sendvia_object')->setData(array(
                    'name'  => 'Obj 1 name'
                ))
            )
        ));

        $this->_model->setHttpResponse(
            $this->_getMockHttpResponse($json)
        );

        $this->assertEquals(
            $expected,
            $this->_model->getItem()
        );
    }

    /**
     * @test
     */
    public function testGetAllItemsRecursivelyConvertsJsonObjectsToSendviaObjects()
    {
        $json = '
            [
                {
                    "Name":"My name",
                    "ArrayOfObjects":[
                        {
                            "Name":"Obj 1 name"
                        }
                    ]
                },
                {
                    "Name":"My second name",
                    "ArrayOfObjects":[
                        {
                            "Name":"second Obj 1 name",
                            "DeeperObject":{
                                "Name":"deeper obj name"
                            }
                        }
                    ]
                }
            ]';

        $expected = array(
            Mage::getModel('mhi_sendvia/sendvia_object')->setData(array(
                'name'              => 'My name',
                'array_of_objects'    => array(
                    Mage::getModel('mhi_sendvia/sendvia_object')->setData(array(
                        'name'  => 'Obj 1 name'
                    ))
                )
            )),

            Mage::getModel('mhi_sendvia/sendvia_object')->setData(array(
                'name'              => 'My second name',
                'array_of_objects'    => array(
                    Mage::getModel('mhi_sendvia/sendvia_object')->setData(array(
                        'name'          => 'second Obj 1 name',
                        'deeper_object'  => Mage::getModel('mhi_sendvia/sendvia_object')->setData(array(
                            'name'          => 'deeper obj name',
                        ))
                    ))
                )
            )),
        );

        $this->_model->setHttpResponse(
            $this->_getMockHttpResponse($json)
        );

        $this->assertEquals(
            $expected,
            $this->_model->getAllItems()
        );
    }

    /**
     * @test
     */
    public function testIsSuccessfulWhenResponseHasStatus200()
    {
        $m = $this->_model;

        $m->setHttpResponse($this->_getMockHttpResponse('', 200));
        $this->assertTrue(
            $m->isSuccess(),
            'isSuccess() should be true when status is 200'
        );
    }

    /**
     * @test
     */
    public function testIsNotSuccessfulWhenResponseHasStatus500()
    {
        $m = $this->_model;

        $m->setHttpResponse($this->_getMockHttpResponse('', 500));
        $this->assertFalse(
            $m->isSuccess(),
            'isSuccess() should be false when status is 500'
        );
    }

    /**
     * @test
     */
    public function testErrorMessageIsEmptyStringWhenResponseSuccessful()
    {
        $mockHttpResponse = $this->_getMockHttpResponse(
            '{"SomeKey":"blah","SomeOtherKey":"more blah"}', 200
        );
        $this->_model->setHttpResponse($mockHttpResponse);

        $errorMsg = $this->_model->getErrorMessage();
        $this->assertInternalType('string', $errorMsg, 'Error message should be a string');
        $this->assertEmpty($errorMsg, 'Error message should be empty');
    }

    /**
     * @test
     */
    public function testErrorMessageReturnsUnknownErrorWhenNotSuccessAndErrorNotInBody()
    {
        $mockHttpResponse = $this->_getMockHttpResponse(
            '{"SomeKey":"blah","SomeOtherKey":"more blah"}', 500
        );
        $this->_model->setHttpResponse($mockHttpResponse);

        $errorMsg = $this->_model->getErrorMessage();
        $this->assertInternalType('string', $errorMsg);
        $this->assertEquals(
            'Unknown Error',
            $errorMsg,
            'Error message should be "Unknown Error"'
        );

    }

    /**
     * @test
     */
    public function testErrorMessageReturnsSendviaErrorWhenNotSuccess()
    {
        $mockHttpResponse = $this->_getMockHttpResponse(
            '{"Message":"Sendvia Error","ExceptionMessage":"Details of exception"}', 500
        );
        $this->_model->setHttpResponse($mockHttpResponse);

        $errorMsg = $this->_model->getErrorMessage();
        $this->assertInternalType('string', $errorMsg);
        $this->assertEquals(
            'Sendvia Error Details of exception',
            $errorMsg,
            'Error message should include sendvia error'
        );
    }


    /**
     * @test
     */
    public function testIsAuthenticationFailureWhenStatusIs401()
    {
        $m = $this->_model;

        $m->setHttpResponse($this->_getMockHttpResponse('', 401));
        $this->assertTrue(
            $m->isAuthenticationFailure(),
            'isAuthenticationFailure() should be true when status is 401'
        );
    }

    /**
     * @test
     */
    public function testIsNotAuthenticationErrorWhenStatusIs500()
    {
        $m = $this->_model;

        $m->setHttpResponse($this->_getMockHttpResponse('', 500));
        $this->assertFalse(
            $m->isAuthenticationFailure(),
            'isAuthenticationFailure() should be false when status is 500'
        );
    }

    /**
     * @test
     */
    public function testIsNotAuthenticationErrorWhenStatusIs200()
    {
        $m = $this->_model;

        $m->setHttpResponse($this->_getMockHttpResponse('', 200));
        $this->assertFalse(
            $m->isAuthenticationFailure(),
            'isAuthenticationFailure() should be false when status is 200'
        );
    }


    /**
     * @param int $status
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function _getMockHttpResponse($body = '', $status = 200)
    {
        $mock = $this->getMockBuilder('Zend_Http_Response')
            ->disableOriginalConstructor()
            ->setMethods(array('getStatus', 'getBody'))
            ->getMock()
        ;
        $mock->expects($this->any())
            ->method('getStatus')
            ->will($this->returnValue($status))
        ;
        $mock->expects($this->any())
            ->method('getBody')
            ->will($this->returnValue($body))
        ;

        return $mock;
    }

}