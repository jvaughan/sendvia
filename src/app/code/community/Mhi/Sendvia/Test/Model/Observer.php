<?php

class Mhi_Sendvia_Test_Model_Observer extends Mhi_Sendvia_Test_Case {

    /**
     * @var Mhi_Sendvia_Model_Observer
     */
    protected $_model;

    public function setUp()
    {
        parent::setUp();
        $this->_model = Mage::getModel('mhi_sendvia/observer');
    }

    public function tearDown()
    {
        unset($this->_model);
        parent::tearDown();
    }

    /**
     * @test
     * @loadFixture
     */
    public function testSalesOrderPlaceAfterCreatesQuote()
    {
        $order = Mage::getModel('sales/order')
            ->setId(100)
            ->setShippingMethod('mhi_sendvia_methodid-1234-1234-1234-123456789012')
            ->setMhiSendviaQuoteId('quoteid1-1234-1234-1234-123456789012')
            ->setShippingAmount(5.55)
        ;

        $mockQuote = $this->getMockBuilder('Mhi_SendVia_Model_Sendvia_Quote')
            ->setMethods(array('load'))
            ->getMock()
        ;
        $mockQuote->expects($this->once())
            ->method('load')
            ->with('quoteid1-1234-1234-1234-123456789012')
            ->will($this->returnValue(
                Mage::getModel('mhi_sendvia/sendvia_quote')
                    ->setData('id', 'quoteid1-1234-1234-1234-123456789012')
            ))
        ;
        $this->replaceByMock(
            'model',
            'mhi_sendvia/sendvia_quote',
            $mockQuote
        );

        $observer = new Varien_Event_Observer();
        $observer->setEvent(new Varien_Object(array('order' => $order)));

        $this->assertCount(
            0,
            Mage::getModel('mhi_sendvia/quote')->getCollection(),
            'Quotes collection should have one item (fixture) at the beginning'
        );

        $this->_model->salesOrderPlaceAfter($observer);

        $collection = Mage::getModel('mhi_sendvia/quote')->getCollection()->addFieldToFilter(
            array('quote_id', 'order_id', 'status', 'sendvia_id', 'cost')
        );
        $this->assertCount(
            1,
            $collection,
            'Quotes collection Should have 2 items after salesOrderPlaceAfterCalled'
        );

        $quote = $collection->getFirstItem();

        $this->assertEquals(
            'quoteid1-1234-1234-1234-123456789012',
            $quote->getSendviaId()
        );

        $this->assertEquals(
            '0',
            $quote->getStatus()
        );

        $this->assertEquals(
            100,
            $quote->getOrderId()
        );

        $this->assertEquals(
            5.55,
            $quote->getCost()
        );
    }
}