<?php

class Mhi_Sendvia_Test_Model_Quote extends Mhi_Sendvia_Test_Case {
    /**
     * @var Mhi_Sendvia_Model_Quote
     */
    protected $_model;

    /**
     * @var Mhi_Sendvia_Model_Sendvia_Booking_Receipt
     */
    protected $_receipt;

    const STATUS_UNBOOKED   = 0;
    const STATUS_BOOKED     = 10;

    public function setUp()
    {
        parent::setUp();
        $this->_model = Mage::getModel('mhi_sendvia/quote');

        $this->_receipt = Mage::getModel('mhi_sendvia/sendvia_booking_receipt')->setData(array(
            'id'            => 'receipt-id',
            'receipt_field' => 'value'
        ));
    }

    public function tearDown()
    {
        unset($this->_model);
        parent::tearDown();
    }

    /**
     * @test
     * @loadFixture
     */
    public function testBookShouldSetStatusAndStoreReceipt()
    {
        $m = $this->_model;
        $m->load(1);
        $this->assertTrue(
            $m->hasData(),
            'Loaded quote fixture should have data'
        );


        $this->replaceByMock(
            'model',
            'mhi_sendvia/sendvia_booking',
            $this->_getMockBooking()
        );

        $m->book();

        $this->assertEquals(
            $this->_receipt,
            $m->getSendviaReceipt(),
            'Receipt has not been stored'
        );

        $this->assertEquals(
            $this->_receipt['id'],
            $m->getReceiptId(),
            'Receipt ID has not been stored correctly'
        );

        $this->assertEquals(
            self::STATUS_BOOKED,
            $m->getStatus(),
            'status should be booked after calling book()'
        );
    }


    /**
     * @test
     */
    public function testGetSendviaQuoteReturnsCorrectQuoteObject()
    {
        $svQuote = Mage::getModel('mhi_sendvia/sendvia_quote')->setData(array(
            'field1'    => 'val1',
            'field2'    => 'val2',
            'sub_object'=> Mage::getModel('mhi_sendvia/sendvia_object')->setData(array(
                    'field1' => 'val1',
                    'field2' => 'val2',
                ))
        ));
        $this->_model->setQuoteData(serialize($svQuote));

        $this->assertEquals(
            $svQuote,
            $this->_model->getSendviaQuote()
        );
    }

    /**
     * @test
     */
    public function testGetSendviaQuoteReturnsNullWhenEmpty()
    {
        $this->assertFalse(
            $this->_model->getSendviaQuote()
        );
    }

    /**
     * @test
     */
    public function testGetSendviaReceiptReturnsCorrectReceiptObject()
    {
        $svReceipt = Mage::getModel('mhi_sendvia/sendvia_booking_receipt')->setData(array(
            'field1'    => 'val1',
            'field2'    => 'val2',
            'sub_object'=> Mage::getModel('mhi_sendvia/sendvia_object')->setData(array(
                    'field1' => 'val1',
                    'field2' => 'val2',
                ))
        ));

        $this->_model->setReceiptData(serialize($svReceipt));

        $this->assertEquals(
            $svReceipt,
            $this->_model->getSendviaReceipt()
        );
    }

    /**
     * @test
     */
    public function testGetBookingIdReturnsValueFromReceiptObject()
    {
        $svReceipt = Mage::getModel('mhi_sendvia/sendvia_booking_receipt')->setData(array(
            'booking_id'    => 'booking-id',
        ));

        $this->_model->setReceiptData(serialize($svReceipt));

        $this->assertEquals(
            'booking-id',
            $this->_model->getBookingId()
        );
    }

    /**
     * @test
     */
    public function testGetBookingIdReturnsNullWhenNoReceipt()
    {
        $this->assertNull(
            $this->_model->getBookingId()
        );
    }

    /**
     * @test
     */
    public function testIsStatusInvalidShouldReturnFalseForValidQuote()
    {
        $this->_model->setStatus(Mhi_Sendvia_Model_Quote::STATUS_UNBOOKED);

        $this->assertFalse(
            $this->_model->isStatusInvalid()
        );
    }

    /**
     * @test
     */
    public function testIsStatusInvalidShouldReturnTrueForInvalidQuote()
    {
        $this->_model->setStatus(Mhi_Sendvia_Model_Quote::STATUS_INVALID);

        $this->assertTrue(
            $this->_model->isStatusInvalid()
        );
    }

    /**
     * @test
     */
    public function testGetSendviaReceiptReturnsNullWhenEmpty()
    {
        $this->assertFalse(
            $this->_model->getSendviaQuote()
        );
    }

    protected function _getMockBooking()
    {
        $mockBooking = $this->getMockBuilder('Mhi_Sendvia_Model_Sendvia_Booking')
            ->setMethods(array('buildFromQuote', 'save', 'getReceipt'))
            ->getMock()
        ;
        $mockBooking->expects($this->once())
            ->method('buildFromQuote')
            ->with($this->_model)
            ->will($this->returnSelf())
        ;
        $mockBooking->expects($this->once())
            ->method('save')
            ->will($this->returnSelf())
        ;
        $mockBooking->expects($this->any())
            ->method('getReceipt')
            ->will($this->returnValue($this->_receipt))
        ;

        return $mockBooking;
    }
}