<?php

class Mhi_Sendvia_Test_Model_Sendvia_Quote_Query extends Mhi_Sendvia_Test_Case {

    /**
     * @var Mhi_Sendvia_Model_Sendvia_Quote_Query
     */
    protected $_model;

    public function setUp()
    {
        parent::setUp();
        $this->_model = Mage::getModel('mhi_sendvia/sendvia_quote_query');
    }

    public function tearDown()
    {
        unset($this->_model);
        parent::tearDown();
    }

    /**
     * @test
     * @dataProvider dataProvider
     */
    public function testBuildFromRateRequest($id, $requestData)
    {
        $this->_replaceWsHelperWithMock();

        $expectedObjects = $this->expected($id)->getObjects();

        $expectedQuery = Mage::getModel('mhi_sendvia/sendvia_quote_query')
            ->setData($expectedObjects['query'])
            ->setShipments(array(
                Mage::getModel('mhi_sendvia/sendvia_shipment')
                    ->setData($expectedObjects['shipment'])
                    ->setParcels(array(
                        Mage::getModel('mhi_sendvia/sendvia_shipment_parcel')
                            ->setData($expectedObjects['parcel'])
                        ,
                    ))
                    ->setSender(
                        Mage::getModel('mhi_sendvia/sendvia_contact')
                            ->setData($expectedObjects['sender_contact'])
                            ->setAddress(
                                Mage::getModel('mhi_sendvia/sendvia_contact_address')
                                    ->setData($expectedObjects['sender_address'])
                            )
                    )
                    ->setRecipient(
                        Mage::getModel('mhi_sendvia/sendvia_contact')
                            ->setData($expectedObjects['recipient_contact'])
                            ->setAddress(
                                Mage::getModel('mhi_sendvia/sendvia_contact_address')
                                    ->setData($expectedObjects['recipient_address'])
                            )
                    )
            ))
        ;

        $requestModel = $this->_getRequestModel($requestData);
        $returned = $this->_model->buildFromRateRequest($requestModel);

        $this->assertInstanceOf(
            'Mhi_Sendvia_Model_Sendvia_Quote_Query',
            $returned
        );

        $this->assertEquals(
            $expectedQuery,
            $this->_model
        );

        foreach ($this->_model->getShipments() as $shipment) {
            foreach ($shipment->getParcels() as $parcel) {
                $this->assertInternalType(
                    'integer',
                    $parcel->getWeight()
                );
            }
        }
    }

    protected function _replaceWsHelperWithMock()
    {
        $mockWsHelper = $this->getMockBuilder('Mhi_SendVia_Helper_Webservice')
            ->setMethods(array('generateGuid'))
            ->getMock()
        ;
        $mockWsHelper->expects($this->any())
            ->method('generateGuid')
            ->will($this->returnValue('generated-guid'))
        ;
        $this->replaceByMock(
            'helper',
            'mhi_sendvia/webservice',
            $mockWsHelper
        );
    }

    /**
     * @param $request_data
     *
     * @return Mage_Shipping_Model_Rate_Request
     */
    protected function _getRequestModel($requestData)
    {
        foreach ($requestData['all_items'] as $k => $item) {
            $requestData['all_items'][$k] = Mage::getModel('sales/quote_item')->setData($item);
        }

        $result = Mage::getModel('shipping/rate_request')->addData($requestData);
        return $result;
    }

}