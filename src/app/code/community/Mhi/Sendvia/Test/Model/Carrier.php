<?php
class Mhi_Sendvia_Test_Model_Carrier extends Mhi_Sendvia_Test_Case {

    /**
     * @var Mhi_Sendvia_Model_Carrier
     */
    protected $_model;

    public function setUp()
    {
        parent::setUp();
        $this->_model = Mage::getModel('mhi_sendvia/carrier');
    }

    public function tearDown()
    {
        unset($this->_model);
        parent::tearDown();
    }

    /**
     * @test
     * @loadFixture
     */
    public function testGetAllowedMethods()
    {
        $returnVal = $this->_model->getAllowedMethods();

        $expected = array(
            'mhi_sendvia_sv-service-1'  => 'carrier 1: service 1',
            'mhi_sendvia_sv-service-2'  => 'carrier 1: service 2',
            'mhi_sendvia_sv-service-3'  => 'carrier 2: service 3',
        );

        $this->assertEquals(
            $expected,
            $returnVal
        );
    }

    /**
     * @test
     * @loadFixture
     * @dataProvider dataProvider
     */
    public function testCollectRates($id, $requestData, $quoteData)
    {
        $rateRequest = Mage::getModel('shipping/rate_request')->setData($requestData);

        $this->_replaceQuoteModelWithMock($rateRequest, $quoteData);

        $expectedResult = $this->_getExpectedRateResult(
            $this->expected($id)->getRateResult()
        );

        $this->assertEquals(
            $expectedResult,
            $this->_model->collectRates($rateRequest),
            'Mage_Shipping_Model_Rate_Result returned by collectRates() does not match expectation'
        );

    }

    /**
     * @test
     */
    public function testCollectRatesReturnsFalseWhenCarrierInactive()
    {

        $mockCarrier = $this->getMockBuilder('Mhi_Sendvia_Model_Carrier')
            ->setMethods(array('isActive'))
            ->getMock()
        ;
        $mockCarrier->expects($this->any())
            ->method('isActive')
            ->will($this->returnValue(false))
        ;
        $this->replaceByMock(
            'model',
            'mhi_sendvia/sendvia_carrier',
            $mockCarrier
        );

        $this->assertFalse(
            $mockCarrier->isActive(),
            'isActive() should return false'
        );

        $rateRequest = Mage::getModel('shipping/rate_request')->setId(1);
        $this->_replaceQuoteModelWithMock($rateRequest, array(), false);

        $this->assertEquals(
            false,
            $mockCarrier->collectRates($rateRequest),
            'collectRates() should return false when carrier is inactive'
        );
    }

    /**
     * @test
     */
    public function testCollectRatesReturnsEmptyResultIfExceptionThrown()
    {
        $expected = Mage::getModel('shipping/rate_result');

        $mockQuote = $this->getMockBuilder('Mhi_SendVia_Model_Sendvia_Quote')
            ->setMethods(array('makeRequest'))
            ->getMock()
        ;
        $mockQuote->expects($this->once())
            ->method('makeRequest')
            ->will($this->throwException(new Mage_Core_Exception()))
        ;
        $this->replaceByMock(
            'model',
            'mhi_sendvia/sendvia_quote',
            $mockQuote
        );

        $result = $this->_model->collectRates(
            Mage::getModel('shipping/rate_request')
        );

        $this->assertEquals(
            $result,
            $expected
        );
    }

    /**
     * @test
     */
    public function testIsTrackingAvailableShouldReturnTrue()
    {
        $this->assertTrue($this->_model->isTrackingAvailable());
    }

    /**
     * @param array $data
     * @return Mage_Shipping_Model_Rate_Result
     */
    protected function _getExpectedRateResult($data)
    {
        $result = Mage::getModel('shipping/rate_result');
        foreach ($data as $method) {
            $result->append(
                Mage::getModel('shipping/rate_result_method')->setData(
                    $method
                )
            );
        }

        return $result;
    }

    /**
     * @param Mage_Shipping_Model_Rate_Request $rateRequest
     * @param array $quoteData
     * @param bool $callMakeRequestOnce
     */
    protected function _replaceQuoteModelWithMock(Mage_Shipping_Model_Rate_Request $rateRequest, $quoteData, $callMakeRequestOnce = true)
    {
        $mockQuote = $this->getMockBuilder('Mhi_SendVia_Model_Sendvia_Quote')
            ->setMethods(array('makeRequest'))
            ->getMock()
        ;
        $mockQuote->expects($callMakeRequestOnce ? $this->once() : $this->any())
            ->method('makeRequest')
            ->with($rateRequest)
            ->will($this->returnValue(
                $mockQuote->setData($this->_prepareDataForQuote($quoteData))
            ))
        ;
        $this->replaceByMock(
            'model',
            'mhi_sendvia/sendvia_quote',
            $mockQuote
        );
    }

    /**
     * @param array $quoteData
     * @return array
     */
    protected function _prepareDataForQuote($quoteData)
    {
        if (! isset($quoteData['quote'])) {
            return array();
        }
        $data = $quoteData['quote'];

        foreach ($quoteData['quote_shipments'] as $shipment) {
            $shipment = Mage::getModel('mhi_sendvia/sendvia_object')
                ->setShipmentId($shipment['shipment_id'])
                ->setCost($shipment['cost'])
                ->setService(
                    Mage::getModel('mhi_sendvia/sendvia_object')
                        ->setData($shipment['service'])
                )
                ->setCarrier(
                    Mage::getModel('mhi_sendvia/sendvia_object')
                        ->setData($shipment['carrier'])
                )
            ;
            $data['quote_shipments'][] = $shipment;

        }

        return $data;
    }

}