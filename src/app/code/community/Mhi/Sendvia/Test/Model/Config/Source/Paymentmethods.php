<?php

class Mhi_Sendvia_Test_Model_Config_Source_Paymentmethods extends Mhi_Sendvia_Test_Case {

    /**
     * @var Mhi_Sendvia_Model_Config_Source_Paymentmethods
     */
    protected $_model;


    public function setUp()
    {
        parent::setUp();
        $this->_model = Mage::getModel('mhi_sendvia/config_source_paymentmethods');
    }

    public function tearDown()
    {
        unset($this->_model);
        parent::tearDown();
    }

    /**
     * @test
     */
    public function testToOptionArray()
    {
        $expected = array(
            array('value'   => 1,   'label'     => 'Paypal'),
            array('value'   => 2,   'label'     => 'Payment Order'),
            array('value'   => 3,   'label'     => 'Invoice'),
        );

        $this->assertEquals(
            $expected,
            $this->_model->toOptionArray()
        );
    }
}