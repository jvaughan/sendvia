<?php

class Mhi_Sendvia_Test_Model_Sendvia_Carrier_Service extends Mhi_Sendvia_Test_Case {

    /**
     * @var Mhi_Sendvia_Model_Sendvia_Carrier_Service
     */
    protected $_model;

    protected $_carrierId = 'bb86e72b-ccf7-422c-9e3a-023196cd476a';

    protected $_serviceId = 'test-service-id';

    /**
     * @var array
     */
    protected $_serviceData = array(
        array(
            'id'        => 'test-service-id',
            'name'      => 'Service Name',
            'private'   => true,
            'transport' => 'transport string',
        ),
        array(
            'id'        => 'test-service-id-2',
            'name'      => 'Service Name 2',
            'private'   => true,
            'transport' => 'transport string 2',
        ),

    );

    public function setUp()
    {
        parent::setUp();
        $this->_model = Mage::getModel('mhi_sendvia/sendvia_carrier_service');
    }

    public function tearDown()
    {
        unset($this->_model);
        parent::tearDown();
    }

    /**
     * @test
     * @covers Mhi_Sendvia_Model_Sendvia_Abstract::load
     * @covers Mhi_Sendvia_Model_Sendvia_Abstract::_getPath
     */
    public function testLoadPopulatesModelWithRequestReadData()
    {
        $m = $this->_model;

        $this->replaceByMock(
            'model',
            'mhi_sendvia/webservice_request',
            $this->_getMockWebserviceRequestModelForReads()
        );

        $this->assertFalse($m->hasData());
        $m->setParent('carrier', $this->_carrierId)
            ->load($this->_serviceId)
        ;
        $this->_testModel($this->_serviceData[0], $m);
    }

    /**
     * @test
     * @covers  Mhi_Sendvia_Model_Sendvia_Abstract::getCollection
     */
    public function testGetCollectionReturnsValidCollectionOfModels()
    {
        $m = $this->_model;

        $this->replaceByMock(
            'model',
            'mhi_sendvia/webservice_request',
            $this->_getMockWebserviceRequestModelForReads()
        );

        $collection = $m->setParent('carrier', $this->_carrierId)
            ->getCollection()
        ;

        $this->assertInstanceOf('Varien_Data_Collection', $collection);
        $this->assertInstanceOf('Mhi_Sendvia_Model_Sendvia_Collection', $collection);

        $this->assertCount(
            count($this->_serviceData),
            $collection
        );

        $i = 0;
        foreach ($collection as $item) {
            $this->_testModel($this->_serviceData[$i++], $item);
        }

    }

    /**
     * @param array $expected
     * @param Mhi_Sendvia_Model_Sendvia_Carrier_Service $model
     */
    protected function _testModel($expected, Mhi_Sendvia_Model_Sendvia_Carrier_Service $model)
    {
        $this->assertInstanceOf('Mhi_Sendvia_Model_Sendvia_Abstract', $model);
        $this->assertInstanceOf('Mhi_Sendvia_Model_Sendvia_Carrier_Service', $model);
        $this->assertTrue($model->hasData());

        $this->assertEquals(
            $expected,
            $model->getData()
        );

        $this->assertEquals(
            $expected['id'],
            $model->getId()
        );
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function _getMockWebserviceRequestModelForReads()
    {
        $mock = $this->getMockBuilder('Mhi_Sendvia_Model_Webservice_Request')
            ->disableOriginalConstructor()
            ->setMethods(array('read'))
            ->getMock()
        ;

        $mock->expects($this->once())
            ->method('read')
            ->will($this->returnCallback(function($arg) {
                $map = array(
                    'carriers/' . $this->_carrierId . '/services' . '/' . $this->_serviceId
                        => $this->_getExampleResponseItemSingle(),

                    'carriers/' . $this->_carrierId . '/services'
                        => $this->_getExampleResponseItemMultiple(),
                );

                return $map[$arg];
            }))
        ;

        return $mock;
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function _getExampleResponseItemSingle()
    {
        $mock = $this->getMockBuilder('Mhi_Sendvia_Model_Webservice_Response')
            ->disableOriginalConstructor()
            ->setMethods(array('getItem'))
            ->getMock()
        ;

        $mock->expects($this->any())
            ->method('getItem')
            ->will($this->returnValue(
                Mage::getModel('mhi_sendvia/sendvia_object', $this->_serviceData[0])
            ));

        return $mock;
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function _getExampleResponseItemMultiple()
    {
        $mock = $this->getMockBuilder('Mhi_Sendvia_Model_Webservice_Response')
            ->disableOriginalConstructor()
            ->setMethods(array('getAllItems'))
            ->getMock()
        ;

        $mock->expects($this->any())
            ->method('getAllItems')
            ->will($this->returnValue(
                array(
                    Mage::getModel('mhi_sendvia/sendvia_object', $this->_serviceData[0]),
                    Mage::getModel('mhi_sendvia/sendvia_object', $this->_serviceData[1]),
                )
            ));

        return $mock;
    }
}