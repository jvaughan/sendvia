<?php

class Mhi_Sendvia_Test_Model_Sendvia_Shipment extends Mhi_Sendvia_Test_Case {

    /**
     * @var Mhi_Sendvia_Model_Sendvia_Shipment
     */
    protected $_model;

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        /*
         * Necessary because of a bug in ecomdev phpunit with setting fixtures
         */
        Mage::unregister('isSecureArea');
        Mage::register('isSecureArea', true);
    }

    public static function tearDownAfterClass()
    {
        /*
        * Necessary because of a bug in ecomdev phpunit with setting fixtures
        */
        Mage::unregister('isSecureArea');
        Mage::register('isSecureArea', false);

        parent::tearDownAfterClass();
    }

    public function setUp()
    {
        parent::setUp();
        $this->_model = Mage::getModel('mhi_sendvia/sendvia_shipment');
    }

    public function tearDown()
    {
        unset($this->_model);
        parent::tearDown();
    }


    /**
     * @test
     * @dataProvider dataProvider
     * @loadFixture
     */
    public function testSandboxValue($store)
    {
        $m = $this->_model;

        /*
        * Necessary because of a bug in ecomdev phpunit with setting fixtures
        */
        $secureArea = Mage::registry('isSecureArea');
        Mage::unregister('isSecureArea');
        Mage::register('isSecureArea', true);

        $this->setCurrentStore($store);
        $expected = (bool) $this->expected($store)->getValue();

        $this->assertEquals(
            $expected,
            $m->getSandbox(),
            'getSandbox() should return ' . ($expected) ? 'true' : 'false'
        );

        $data = $m->getData();
        $this->assertArrayHasKey(
            'sandbox',
            $data
        );

        $this->assertInternalType(
            'bool',
            $data['sandbox']
        );

        $m->setData(array(
            'field1'    => 'val1',
            'field2'    => 'val2',
        ));

        $data = $m->getData();
        $this->assertArrayHasKey(
            'sandbox',
            $data
        );

        $this->assertInternalType(
            'bool',
            $data['sandbox']
        );

        $this->assertEquals(
            array(
                'field1'    => 'val1',
                'field2'    => 'val2',
                'sandbox'   => $expected,
            ),
            $m->getData(),
            'getData() does not return all fields + sandbox'
        );

        $this->assertInternalType(
            'bool',
            $m->getData('sandbox'),
            "getData('sandbox') should return a bool"
        );

        $this->assertEquals(
            $expected,
            $m->getData('sandbox')
        );

        Mage::unregister('isSecureArea');
        Mage::register('isSecureArea', $secureArea);
    }

    /**
     * @test
     */
    public function testGiveNewIdSetsValidGuidAndReturnsSelf()
    {
        $m = $this->_model;

        $this->assertFalse(
            $m->hasData()
        );

        $result = $m->giveNewId();

        $this->assertIsValidGuid(
            $m->getId(),
            "Object id " . $m->getId() . ' is not a valid guid'
        );

        $this->assertInstanceOf(
            'Mhi_Sendvia_Model_Sendvia_Shipment',
            $result
        );
    }

    /**
     * @test
     */
    public function testGetShippingLabelPdfReturnsPdfData()
    {
        $this->replaceByMock(
            'model',
            'mhi_sendvia/webservice_request',
            $this->_getMockWebserviceRequestModelForLabelRead()
        );

        $this->_model->setId('shipment-id');
        $pdf = $this->_model->getShippingLabelPdf();

        $this->assertEquals(
            'pdf-data-string',
            $pdf
        );
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function _getMockWebserviceRequestModelForLabelRead()
    {
        $mock = $this->getMockBuilder('Mhi_Sendvia_Model_Webservice_Request')
            ->disableOriginalConstructor()
            ->setMethods(array('read'))
            ->getMock()
        ;

        $mock->expects($this->once())
            ->method('read')
            ->with('shipments/shipment-id/label')
            ->will($this->returnValue($this->_getMockResponseModel()))
        ;

        return $mock;
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function _getMockResponseModel()
    {
        $base64String = 'cGRmLWRhdGEtc3RyaW5n'; // 'pdf-data-string'
        $mock = $this->getMockBuilder('Mhi_Sendvia_Model_Webservice_Response')
            ->disableOriginalConstructor()
            ->setMethods(array('getAllItems'))
            ->getMock()
        ;

        $mock->expects($this->any())
            ->method('getAllItems')
            ->will($this->returnValue(array(
                Mage::getModel('mhi_sendvia/sendvia_object', array('data'   => $base64String))
            ))
        );

        return $mock;
    }
}