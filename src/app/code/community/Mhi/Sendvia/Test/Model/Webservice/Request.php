<?php

class Mhi_Sendvia_Test_Model_Webservice_Request extends Mhi_Sendvia_Test_Case {

    /**
     * @var Mhi_Sendvia_Model_Webservice_Request
     */
    protected $_model;

    public function setUp()
    {
        parent::setUp();
        $this->_model = Mage::getModel('mhi_sendvia/webservice_request');
    }

    public function tearDown()
    {
        unset($this->_model);
        parent::tearDown();
    }

    /**
     * @test
     * @dataProvider provider
     * @covers Mhi_Sendvia_Model_Webservice_Request::read
     * @covers Mhi_Sendvia_Model_Webservice_Request::create
     * @covers Mhi_Sendvia_Model_Webservice_Request::_makeRequest
     */
    public function testCrudMethodCallsCorrectRestMethodWithPathAndData($method, $data, $processedData, $restMethod)
    {
        $this->_replaceWsHelperWithMock($restMethod, $processedData);
        $this->_model->$method('mypath', $data);
    }

    /**
     * @test
     * @dataProvider provider
     */
    public function testCrudMethodReturnsResponseObject($method, $data, $processedData, $restMethod)
    {
        $this->_replaceWsHelperWithMock($restMethod, $processedData);

        $result = $this->_model->$method('mypath', $data);
        $this->assertInstanceOf(
            'Mhi_Sendvia_Model_Webservice_Response',
            $result
        );
    }

    /**
     * @test
     * @dataProvider provider
     * @expectedException Mage_Core_Exception
     * @expectedExceptionMessage Sendvia request failed: Unknown Error
     *
     * @covers Mhi_Sendvia_Model_Webservice_Request::read
     * @covers Mhi_Sendvia_Model_Webservice_Request::create
     * @covers Mhi_Sendvia_Model_Webservice_Request::_makeRequest
     */
    public function testCrudMethodThrowsExceptionIfResponseNotSuccess($method, $data, $processedData, $restMethod)
    {
        $this->_replaceWsHelperWithMock($restMethod, $processedData, 500);
        $this->_model->$method('mypath', $data);
    }

    /**
     * @test
     * @dataProvider provider
     * @expectedException Mage_Core_Exception
     * @expectedExceptionMessage Sendvia authentication failed: Unknown Error
     *
     * @covers Mhi_Sendvia_Model_Webservice_Request::read
     * @covers Mhi_Sendvia_Model_Webservice_Request::create
     * @covers Mhi_Sendvia_Model_Webservice_Request::_makeRequest
     */
    public function testCrudMethodCallsGetTokenTwiceIfFirstAuthFails($method, $data, $processedData, $restMethod)
    {
        $this->_replaceWsHelperWithMock($restMethod, $processedData, 401, true);
        $this->_model->$method('mypath', $data);
    }

    /**
     * @test
     * @dataProvider provider
     * @expectedException Mage_Core_Exception
     * @expectedExceptionMessage Sendvia authentication failed: Unknown Error
     *
     * @covers Mhi_Sendvia_Model_Webservice_Request::read
     * @covers Mhi_Sendvia_Model_Webservice_Request::create
     * @covers Mhi_Sendvia_Model_Webservice_Request::_makeRequest
     */
    public function testCrudMethodThrowsAuthExceptionIfUnauthorized($method, $data, $processedData, $restMethod)
    {
        $this->_replaceWsHelperWithMock($restMethod, $processedData, 401, true);
        $this->_model->$method('mypath', $data);
    }

    /**
     * @test
     * @dataProvider provider
     *
     * @covers Mhi_Sendvia_Model_Webservice_Request::read
     * @covers Mhi_Sendvia_Model_Webservice_Request::create
     * @covers Mhi_Sendvia_Model_Webservice_Request::_makeRequest
     */
    public function testResponseReturnedWhenFirstAuthFailsButSecondPasses($method, $data, $processedData, $restMethod)
    {
        $this->_replaceWsHelperWithMock($restMethod, $processedData, 401, true, true);
        $result = $this->_model->$method('mypath', $data);

        $this->assertInstanceOf(
            'Mhi_Sendvia_Model_Webservice_Response',
            $result
        );
    }

    /**
     * @param int $status
     * @param bool $callAnyTimes
     */
    protected function _replaceWsHelperWithMock(
        $restMethod = 'restGet',
        $data,
        $status = 200,
        $authFailure = false,
        $secondAuthIsSuccess = false
    )
    {
        $mockRestClient = $this->getMockBuilder('Zend_Rest_Client')
            ->disableOriginalConstructor()
            ->setMethods(array($restMethod))
            ->getMock()
        ;


        if ($secondAuthIsSuccess) {
            $mockRestClient->expects($this->exactly(2))
                ->method($restMethod)
                ->with(
                    $this->equalTo('rest/alpha4/mypath'),
                    $this->equalTo($data)
                )
                ->will($this->onConsecutiveCalls(
                    new Zend_Http_Response(401, array()),
                    new Zend_Http_Response(200, array())
                ))
            ;
        }
        else {
            $mockRestClient->expects($authFailure ? $this->exactly(2) : $this->once())
                ->method($restMethod)
                ->with(
                    $this->equalTo('rest/alpha4/mypath'),
                    $this->equalTo($data)
                )
                ->will($this->returnValue(
                    new Zend_Http_Response($status, array())
                ))
            ;

        }

        $mockWsHelper = $this->getMockBuilder('Mhi_SendVia_Helper_Webservice')
            ->setMethods(array('getRestClient', 'getToken'))
            ->getMock()
        ;
        $mockWsHelper->expects($this->any())
            ->method('getRestClient')
            ->will($this->returnValue($mockRestClient))
        ;
        if ($authFailure) {
            $mockWsHelper->expects($this->exactly(2))
                ->method('getToken')
                ->withConsecutive(
                    array($this->equalTo(false)),
                    array($this->equalTo(true))
                )
                ->will($this->returnValue('bad-token'))
            ;
        }
        else {
            $mockWsHelper->expects($this->once())
                ->method('getToken')
                ->will($this->returnValue('mytoken'))
            ;
        }
        $this->replaceByMock(
            'helper',
            'mhi_sendvia/webservice',
            $mockWsHelper
        );
    }

    /**
     * @return array
     */
    public function provider()
    {
        return array(
            //    CRUD method | data      | rest method
            array('read', array(), array(), 'restGet'),
            array('read', array('thing1' => 'val1'),  array('thing1' => 'val1'),  'restGet'),

            array(
                'create',
                array(
                    'field1' => 'val1',
                    'subObject' => new Varien_Object(array('subfield1'  => 'val2'))
                ),
                '{"Field1":"val1","SubObject":{"Subfield1":"val2"}}',
                'restPost'
            ),
        );
    }
}