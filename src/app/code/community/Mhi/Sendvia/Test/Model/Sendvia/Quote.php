<?php

class Mhi_Sendvia_Test_Model_Sendvia_Quote extends Mhi_Sendvia_Test_Case {

    /**
     * @var Mhi_Sendvia_Model_Sendvia_Quote
     */
    protected $_model;

    /**
     * @var array
     */
    protected $_quoteData = array(
        'queryId'       => 'query-id',
        'currency'      => 100,
    );

    /**
     * @var array
     */
    protected $_queryData = array(
        'currency'      => 100,
        'shipments'     => array(
            'reference'     => 'ship-ref',
        )
    );

    const GENERATED_GUID    = 'generated-guid';

    public function setUp()
    {
        parent::setUp();
        $this->_model = Mage::getModel('mhi_sendvia/sendvia_quote');
    }

    public function tearDown()
    {
        unset($this->_model);
        parent::tearDown();
    }

    /**
     * @test
     */
    public function testSavingQuotePopulatesModelWithResponseData() {
        $m = $this->_model;

        $this->replaceByMock(
            'model',
            'mhi_sendvia/webservice_request',
            $this->_getMockWebserviceRequestModelForCreate()
        );

        $this->_replaceWsHelperWithMock();

        $m->setQuery(Mage::getModel('mhi_sendvia/sendvia_quote_query', $this->_queryData));
        $m->save();

        $expectedData = $this->_quoteData;
        $expectedData['id'] = self::GENERATED_GUID;

        $this->assertEquals(
            $expectedData,
            $m->getData(),
            'Quote model not populated with expected data'
        );
    }

    /**
     * @test
     */
    public function testMakeRequestSavesQueryAndReturnsSelf()
    {
        $rateRequest = Mage::getModel('shipping/rate_request')->setId(1);

        $mockQuote = $this->getMockBuilder('Mhi_SendVia_Model_Sendvia_Quote')
            ->setMethods(array('setQuery', 'save'))
            ->getMock()
        ;
        $mockQuote->expects($this->once())
            ->method('setQuery')
            ->with($this->anything())
            ->will($this->returnSelf());
        ;
        $mockQuote->expects($this->once())
            ->method('save')
            ->will($this->returnSelf());
        ;
        $this->replaceByMock(
            'model',
            'mhi_sendvia/sendvia_quote',
            $mockQuote
        );

        $mockQuery = $this->getMockBuilder('Mhi_SendVia_Model_Sendvia_Quote_Query')
            ->setMethods(array('buildFromRateRequest'))
            ->getMock()
        ;
        $mockQuery->expects($this->once())
            ->method('buildFromRateRequest')
            ->with($rateRequest)
            ->will($this->returnSelf());
        ;

        $this->replaceByMock(
            'model',
            'mhi_sendvia/sendvia_quote_query',
            $mockQuery
        );


        $returned = $mockQuote->makeRequest($rateRequest);
        $this->assertInstanceOf(
            'Mhi_Sendvia_Model_Sendvia_Quote',
            $returned
        );
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function _getMockWebserviceRequestModelForCreate()
    {
        $queryData = $this->_queryData;
        $queryData['id'] = self::GENERATED_GUID;

        $mock = $this->getMockBuilder('Mhi_Sendvia_Model_Webservice_Request')
            ->disableOriginalConstructor()
            ->setMethods(array('create'))
            ->getMock()
        ;

        $mock->expects($this->once())
            ->method('create')
            ->with('quotes', $queryData)
            ->will($this->returnValue($this->_getMockResponseModel()))
        ;

        return $mock;
    }

    /**
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function _getMockResponseModel()
    {
        $mock = $this->getMockBuilder('Mhi_Sendvia_Model_Webservice_Response')
            ->disableOriginalConstructor()
            ->setMethods(array('getItem'))
            ->getMock()
        ;

        $mock->expects($this->any())
            ->method('getItem')
            ->will($this->returnValue(
                Mage::getModel('mhi_sendvia/sendvia_object', $this->_quoteData)
                    ->setId(self::GENERATED_GUID)
            ));

        return $mock;
    }

    /**
     * @param int $status
     * @param bool $callAnyTimes
     */
    protected function _replaceWsHelperWithMock()
    {
        $mockWsHelper = $this->getMockBuilder('Mhi_SendVia_Helper_Webservice')
            ->setMethods(array('generateGuid'))
            ->getMock()
        ;
        $mockWsHelper->expects($this->any())
            ->method('generateGuid')
            ->will($this->returnValue(self::GENERATED_GUID))
        ;
        $this->replaceByMock(
            'helper',
            'mhi_sendvia/webservice',
            $mockWsHelper
        );
    }
}