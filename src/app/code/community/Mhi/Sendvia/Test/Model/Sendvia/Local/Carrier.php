<?php

class Mhi_Sendvia_Test_Model_Sendvia_Local_Carrier extends Mhi_Sendvia_Test_Case {

    /**
     * @var Mhi_Sendvia_Model_Sendvia_Local_Carrier
     */
    protected $_model;


    public function setUp()
    {
        parent::setUp();
        $this->_model = Mage::getModel('mhi_sendvia/sendvia_local_carrier');
    }

    public function tearDown()
    {
        unset($this->_model);
        parent::tearDown();
    }

    /**
     * @test
     * @loadFixture
     * @dataProvider provider
     */
    public function testGetServicesReturnsOnlyChildServices($carrierId, $expectedServiceIds)
    {
        $serviceIds = $this->_model->load($carrierId)->getServices()->getAllIds();

        $this->assertEquals(
            $expectedServiceIds,
            $serviceIds,
            "Returned service Ids for carrier id $carrierId not expected"
        );
    }

    public function provider() {
        return array(
            array(1, array(1,2)),
            array(2, array(3))
        );
    }
}