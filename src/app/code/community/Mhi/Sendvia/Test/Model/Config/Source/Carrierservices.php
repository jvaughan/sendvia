<?php

class Mhi_Sendvia_Test_Model_Config_Source_Carrierservices extends Mhi_Sendvia_Test_Case {


    /**
     * @var Mhi_Sendvia_Test_Model_Config_Source_Carrierservices
     */
    protected $_model;


    public function setUp()
    {
        parent::setUp();
        $this->_model = Mage::getModel('mhi_sendvia/config_source_carrierservices');
    }

    public function tearDown()
    {
        unset($this->_model);
        parent::tearDown();
    }

    /**
     * @test
     * @loadFixture
     * @dataProvider dataprovider
     */
    public function testToOptionArray($id, $configPath)
    {
        $expected = $this->expected($id)->getResult();

        if ($configPath) {
            $this->_model->setPath($configPath);
        }

        $result = $this->_model->toOptionArray();
        $this->assertEquals(
            $expected,
            $result
        );
    }


}