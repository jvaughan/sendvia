<?php

class Mhi_Sendvia_Test_Config_Main extends EcomDev_PHPUnit_Test_Case_Config
{

    protected $_moduleName = 'Mhi_Sendvia';
    protected $_moduleVersion = '0.1.0';
    protected $_codePool = 'community';

    protected $_modelAliases = array(
        'mhi_sendvia/carrier'                       => 'Mhi_Sendvia_Model_Carrier',
        'mhi_sendvia/config_source_carrierservices' => 'Mhi_Sendvia_Model_Config_Source_Carrierservices',

        'mhi_sendvia/observer'                       => 'Mhi_Sendvia_Model_Observer',

        'mhi_sendvia/sendvia_abstract'              => 'Mhi_Sendvia_Model_Sendvia_Abstract',
        'mhi_sendvia/sendvia_carrier'               => 'Mhi_Sendvia_Model_Sendvia_Carrier',
        'mhi_sendvia/sendvia_collection'            => 'Mhi_Sendvia_Model_Sendvia_Collection',
        'mhi_sendvia/sendvia_contact'               => 'Mhi_Sendvia_Model_Sendvia_Contact',
        'mhi_sendvia/sendvia_contact_address'       => 'Mhi_Sendvia_Model_Sendvia_Contact_Address',
        'mhi_sendvia/sendvia_object'                => 'Mhi_Sendvia_Model_Sendvia_Object',

        'mhi_sendvia/sendvia_quote'                 => 'Mhi_Sendvia_Model_Sendvia_Quote',
        'mhi_sendvia/sendvia_quote_query'           => 'Mhi_Sendvia_Model_Sendvia_Quote_Query',
        'mhi_sendvia/sendvia_shipment'              => 'Mhi_Sendvia_Model_Sendvia_Shipment',
        'mhi_sendvia/sendvia_shipment_parcel'       => 'Mhi_Sendvia_Model_Sendvia_Shipment_Parcel',
        'mhi_sendvia/sendvia_shipment_parcel_milestone'
                                                    => 'Mhi_Sendvia_Model_Sendvia_Shipment_Parcel_Milestone',

        'mhi_sendvia/sendvia_booking'               => 'Mhi_Sendvia_Model_Sendvia_Booking',
        'mhi_sendvia/sendvia_booking_receipt'       => 'Mhi_Sendvia_Model_Sendvia_Booking_Receipt',

        'mhi_sendvia/quote'                         => 'Mhi_Sendvia_Model_Quote',
        'mhi_sendvia/resource_quote'                => 'Mhi_Sendvia_Model_Resource_Quote',
        'mhi_sendvia/resource_quote_collection'     => 'Mhi_Sendvia_Model_Resource_Quote_Collection',

        'mhi_sendvia/sendvia_local_carrier'         => 'Mhi_Sendvia_Model_Sendvia_Local_Carrier',
        'mhi_sendvia/sendvia_local_carrier_service' => 'Mhi_Sendvia_Model_Sendvia_Local_Carrier_Service',

        'mhi_sendvia/resource_sendvia_local_carrier'=> 'Mhi_Sendvia_Model_Resource_Sendvia_Local_Carrier',
        'mhi_sendvia/resource_sendvia_local_carrier_service'
                                                    => 'Mhi_Sendvia_Model_Resource_Sendvia_Local_Carrier_Service',

        'mhi_sendvia/resource_sendvia_local_carrier_collection'
                                                    => 'Mhi_Sendvia_Model_Resource_Sendvia_Local_Carrier_Collection',
        'mhi_sendvia/resource_sendvia_local_carrier_service_collection'
                                                    => 'Mhi_Sendvia_Model_Resource_Sendvia_Local_Carrier_Service_Collection',

        'mhi_sendvia/rest_client'                   => 'Mhi_Sendvia_Model_Rest_Client',

        'mhi_sendvia/webservice_request'            => 'Mhi_Sendvia_Model_Webservice_Request',
        'mhi_sendvia/webservice_request_authentication'
                                                    => 'Mhi_Sendvia_Model_Webservice_Request_Authentication',

        'mhi_sendvia/webservice_response'           => 'Mhi_Sendvia_Model_Webservice_Response',
    );

    protected $_helperAliases = array(
        'mhi_sendvia'                               => 'Mhi_Sendvia_Helper_Data',
        'mhi_sendvia/cache'                         => 'Mhi_Sendvia_Helper_Cache',
        'mhi_sendvia/config'                        => 'Mhi_Sendvia_Helper_Config',
        'mhi_sendvia/configxml'                     => 'Mhi_Sendvia_Helper_Configxml',
        'mhi_sendvia/debug'                         => 'Mhi_Sendvia_Helper_Debug',
        'mhi_sendvia/locale'                        => 'Mhi_Sendvia_Helper_Locale',
        'mhi_sendvia/order'                         => 'Mhi_Sendvia_Helper_Order',
        'mhi_sendvia/quote'                         => 'Mhi_Sendvia_Helper_Quote',
        'mhi_sendvia/sendvia'                       => 'Mhi_Sendvia_Helper_Sendvia',
        'mhi_sendvia/webservice'                    => 'Mhi_Sendvia_Helper_Webservice',
    );

    protected $_configValues = array(
        'default/carriers/mhi_sendvia/model'
            => 'mhi_sendvia/carrier',

        'default/carriers/mhi_sendvia/payment_method'
            => 1,

        'frontend/routers/mhi_sendvia/use'              => 'standard',
        'frontend/routers/mhi_sendvia/args/module'      => 'Mhi_Sendvia',
        'frontend/routers/mhi_sendvia/args/frontName'   => 'sendvia',

    );

    /**
     * @test
     */
    public function testModuleConfiguration()
    {
        $this->assertModuleVersion($this->_moduleVersion);
        $this->assertModuleIsActive($this->_moduleName);
        $this->assertModuleCodePool($this->_codePool, $this->_moduleName);
    }

    /**
     * @test
     */
    public function testModelAliases()
    {
        foreach ($this->_modelAliases as $alias => $className) {
            $this->assertModelAlias($alias, $className);

            if (!preg_match('/abstract/', $alias)) {
                $this->assertEquals(
                    $className,
                    get_class(Mage::getModel($alias)),
                    "Couldn't get class of getModel($alias)"
                );
            }
        }
    }

    /**
     * @test
     */
    public function testHelperAliases()
    {
        foreach ($this->_helperAliases as $alias => $className) {
            $this->assertHelperAlias($alias, $className);

            if (!preg_match('/abstract/', $alias)) {
                $this->assertEquals(
                    $className,
                    get_class(Mage::helper($alias)),
                    "Couldn't get class of helper($alias)"
                );
            }
        }
    }

    /**
     * @test
     */
    public function testDefaultConfigValues()
    {
        foreach ($this->_configValues as $node => $expValue) {
            $this->assertConfigNodeContainsValue($node, $expValue);
        }
    }

    /**
     * @test
     */
    public function testAdminhtml_init_system_configObserverDefined()
    {
        $this->assertEventObserverDefined(
            'adminhtml',
            'adminhtml_init_system_config',
            'mhi_sendvia/observer',
            'adminhtmlInitSystemConfig'
        );
    }

    /**
     * @test
     */
    public function testSalesOrderPlaceAfterObserverDefined()
    {
        $this->assertEventObserverDefined(
            'global',
            'sales_order_place_after',
            'mhi_sendvia/observer',
            'salesOrderPlaceAfter'
        );
    }

    /**
     * @test
     */
    public function testsalesQuoteAddressSaveBeforeObserverDefined()
    {
        $this->assertEventObserverDefined(
            'global',
            'sales_quote_address_save_before',
            'mhi_sendvia/observer',
            'salesQuoteAddressSaveBefore'
        );
    }


    /**
     * @test
     */
    public function testSalesOrderShipmentSaveAfterObserverDefined()
    {
        $this->assertEventObserverDefined(
            'global',
            'sales_order_shipment_save_after',
            'mhi_sendvia/observer',
            'salesOrderShipmentSaveAfter'
        );
    }

}