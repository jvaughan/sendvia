<?php

require_once('app/Mage.php'); Mage::app();

$query = Mage::getModel('mhi_sendvia/sendvia_quote_query');

$query->setData(array(
    'currency'  => 826,
    'shipments' => array(
        Mage::getModel('mhi_sendvia/sendvia_shipment')->giveNewId()->addData(array(
            // Shipment
            'collection'    => '2014-09-07',
            'parcels'       => array(
                Mage::getModel('mhi_sendvia/sendvia_shipment_parcel')->giveNewId()->addData(array(
                    // Parcel
                    'weight'    => 100,
                )),
            ),
            'sender'        => Mage::getModel('mhi_sendvia/sendvia_contact')->giveNewId()->addData(array(
                'phone_number'  => '0123456789',
                'mobile_number' => '0123456789',
                // Sender contact
                'address'       => Mage::getModel('mhi_sendvia/sendvia_contact_address')->giveNewId()->addData(array(
                    'countryiso'    => 826,
                )), // sender address
            )), // sender contact
            'recipient'        => Mage::getModel('mhi_sendvia/sendvia_contact')->giveNewId()->addData(array(
                'phone_number'  => '0123456789',
                'mobile_number' => '0123456789',
                // Recipient contact
                'address'           => Mage::getModel('mhi_sendvia/sendvia_contact_address')->giveNewId()->addData(array(
                    'countryiso'    => 826,
                )) // recipient address
            )) //recipient contact
        )) // shipment
    ) // Shipments array
));

$quote = Mage::getModel('mhi_sendvia/sendvia_quote');
$quote->setQuery($query);
$quote->save();

print_r($quote->getData());

