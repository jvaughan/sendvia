<?php

require_once('app/Mage.php'); Mage::app();

$carrierModel = Mage::getModel('mhi_sendvia/sendvia_carrier');

$carrierIdToLoad = null;

foreach ($carrierModel->getCollection() as $carrier) {
    echo "Carrier '" . $carrier->getName() . "':\n";
    print_r($carrier->getData());

    if (!$carrierIdToLoad)  {
        $carrierIdToLoad = $carrier->getId();
    }

    echo "Its services: \n";
    $serviceModel = Mage::getModel('mhi_sendvia/sendvia_carrier_service');
    $serviceModel->setParent('carrier', $carrier->getId());
    foreach ($serviceModel->getCollection() as $service) {
        print_r($service->getData());
    }
}

$carrierModel->load($carrierIdToLoad);
echo "First Carrier returned loaded directly:\n";
print_r($carrierModel->getData());