#!/bin/sh

n98-magerun.phar db:query "DELETE FROM core_resource WHERE code = 'mhi_sendvia_setup'; DROP TABLE IF EXISTS mhi_sendvia_local_carrier_service, mhi_sendvia_local_carrier, mhi_sendvia_quote; ALTER TABLE sales_flat_quote_address DROP COLUMN mhi_sendvia_query_id, DROP COLUMN mhi_sendvia_quote_id; ALTER TABLE sales_flat_order DROP COLUMN mhi_sendvia_query_id, DROP COLUMN mhi_sendvia_quote_id;"
